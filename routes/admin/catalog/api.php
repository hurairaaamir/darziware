<?php

use Illuminate\Support\Facades\Route;


Route::group(['prefix' => '/api/admin/catalog-master', 'middleware' => ['auth:api']], function () {
    Route::post('/add-catalog', 'Admin\Catalog\CatalogController@addCatalog');
    Route::post('/update-catalog/{id}', 'Admin\Catalog\CatalogController@updateCatalog');
    Route::delete('/remove-catalog/{id}', 'Admin\Catalog\CatalogController@removeCatalog');
    Route::get('/get-catalogs', 'Admin\Catalog\CatalogController@getCatalogs');
    Route::get('/get-items', 'Admin\Catalog\CatalogController@getItems');
    Route::post('/status-catalog', 'Admin\Catalog\CatalogController@catalogStatus');
});
