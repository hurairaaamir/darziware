<?php

use Illuminate\Support\Facades\Route;


Route::group(['prefix' => '/api/admin/attribute-master', 'middleware' => ['auth:api']], function () {
    Route::post('/add-attribute', 'Admin\Product\ProductAttributeController@addProductAttribute');
    Route::post('/update-attribute/{id}', 'Admin\Product\ProductAttributeController@updateProductAttribute');
    Route::delete('/remove-attribute/{id}', 'Admin\Product\ProductAttributeController@removeProductAttribute');
    Route::get('/get-attributes', 'Admin\Product\ProductAttributeController@getProductAttributes');
    Route::get('/get-attribute-items', 'Admin\Product\ProductAttributeController@getAttributeItems');
});
