<?php

use Illuminate\Support\Facades\Route;


Route::group(['prefix' => '/api/admin/product-master', 'middleware' => ['auth:api']], function () {
    Route::post('/add-product', 'Admin\Product\ProductController@addProduct');
    Route::post('/update-product/{id}', 'Admin\Product\ProductController@updateProduct');
    Route::delete('/remove-product/{id}', 'Admin\Product\ProductController@removeProduct');
    Route::get('/get-products', 'Admin\Product\ProductController@getProducts');
    Route::get('/get-catalogs', 'Admin\Product\ProductController@getCatalogs');
    Route::get('/get-items', 'Admin\Product\ProductController@getItems');
    Route::get('/get-item-catalogs/{itemId}', 'Admin\Product\ProductController@getItemCatalogs');
    Route::get('/get-item-attributes/{itemId}', 'Admin\Product\ProductController@getItemAttributes');
    Route::post('/status-product', 'Admin\Product\ProductController@productStatus');
});
