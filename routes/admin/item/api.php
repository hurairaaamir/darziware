<?php

use Illuminate\Support\Facades\Route;


Route::group(['prefix' => '/api/admin/item-master', 'middleware' => ['auth:api']], function () {
    Route::post('/add-item', 'Admin\Item\ItemController@addItem');
    Route::post('/update-item/{id}', 'Admin\Item\ItemController@updateItem');
    Route::delete('/remove-item/{id}', 'Admin\Item\ItemController@removeItem');
    Route::get('/get-items', 'Admin\Item\ItemController@getItems');
    Route::post('/status-item', 'Admin\Item\ItemController@statusItem');
});

Route::get('/api/admin/item-master/get-dw-items', 'Admin\Item\ItemController@getDWItems');

