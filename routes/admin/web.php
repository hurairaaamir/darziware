<?php

use Illuminate\Support\Facades\Route;


Route::group(['prefix' => '/admin', 'middleware' => ['auth:admin']], function () {

    Route::view('/edit-profile', 'admin.auth.edit-profile');
    Route::view('/change-password', 'admin.auth.change-password');
    Route::post('/update-profile', [
        'uses' => 'Admin\AdminController@updateProfile',
        'as' => 'admin.update.profile'
    ]);
    Route::post('/update-password', [
        'uses' => 'Admin\AdminController@updatePassword',
        'as' => 'admin.update.password'
    ]);
    Route::get('/logout', [
        'uses' => 'Admin\AdminController@logout',
        'as' => 'admin.logout'
    ]);

    Route::get('/', function () {
        return view('admin.index');
    });
});


// Route::namespace('Admin')->prefix('admin')->as('admin.')->group(function () {
//     Auth::routes(['register' => false]);
// });

// Route::group(['prefix' => '/admin', 'middleware' => ['auth:api']], function () {
//     Route::view('{any}', 'application')->where('any', '.*');
// });

Route::view('{any}', 'application')->where('any', '.*');
