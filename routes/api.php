<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => '/news-letter'], function () {
    Route::post('/store','NewsLetterController@store');
    Route::post('/index','NewsLetterController@index');
    Route::post('/delete','NewsLetterController@destroy');
});





Route::post('/login', 'AuthenController@login')->name('login');
Route::post('/register', 'AuthenController@register')->name('register');

Route::post('/logout', 'AuthenController@logout');

// Route::middleware(['auth:api', 'role'])->group(function () {
//     // List users
//     Route::middleware(['scope:admin'])->get('/users', function (Request $request) {

//         return 'Working';
//     });
// });
