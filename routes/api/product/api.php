<?php

use Illuminate\Support\Facades\Route;


Route::group(['prefix' => '/product-master'], function () {
    Route::get('/get-products', 'Api\Product\ProductController@getProducts');
    Route::get('/get-product/{id}', 'Api\Product\ProductController@getProduct');
    Route::get('/get-catalog-products/{catalog_id}', 'Api\Product\ProductController@getCatalogProducts');
    Route::get('/get-search-products/{search}', 'Api\Product\ProductController@getSearchProducts');
});
