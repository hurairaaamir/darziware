<?php

use Illuminate\Support\Facades\Route;



Route::group(['prefix' => '/item-master'], function () {
    Route::get('/get-items', 'Api\Item\ItemController@getItems');
    Route::get('/get-men-items', 'Api\Item\ItemController@getMenItems');
    Route::get('/get-women-items', 'Api\Item\ItemController@getWomenItems');
});
