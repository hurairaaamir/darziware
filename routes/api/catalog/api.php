<?php

use Illuminate\Support\Facades\Route;


Route::group(['prefix' => '/catalog-master'], function () {
    Route::get('/get-catalogs', 'Api\Catalog\CatalogController@getCatalogs');
});
