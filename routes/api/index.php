<?php

use Illuminate\Support\Facades\Route;


Route::group(['prefix' => ''], function () {
    Route::post('/contact-us', 'Api\ApiController@contactUs');
    Route::post('/contact-us/index', 'Api\ApiController@index'); 
});
