<?php

namespace App\Models\Admin\Catalog;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Catalog extends Model
{
    use HasFactory;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description', 'item_id','status'
    ];



    public function products()
    {
        return $this->hasMany('App\Models\Admin\Product\Product');
    }

    public function item()
    {
        return $this->belongsTo('App\Models\Admin\Item\Item');
    }
}
