<?php

namespace App\Models\Admin\Product;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductProductAttribute extends Model
{
    use HasFactory;
    protected $fillable = [
        'value', 'product_id', 'product_attribute_id'
    ];
}
