<?php

namespace App\Models\Admin\Product;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class Product extends Model
{
    use HasFactory;
    protected $fillable = [
        'title', 'image_url', 'mirror_image_url', 'on_sale', 'new_product', 'offer', 'discount', 'old_price', 'new_price', 'discount_off', 'description', 'catalog_id', 'catalog_name', 'item_id', 'item_name','status'
    ];

    public function getNextId()
    {
        $statement = DB::select("show table status like 'products'");
        return $statement[0]->Auto_increment;
    }

    public static function scopeSearch($query, $searchTerm)
    {
        return $query->where('title', 'like', '%' . $searchTerm . '%');
    }

    public function catalog()
    {
        return $this->belongsTo('App\Models\Admin\Catalog\Catalog');
    }

    public function product_attributes()
    {
        return $this->belongsToMany('App\Models\Admin\Product\ProductAttribute', 'product_product_attributes', 'product_id', 'product_attribute_id')->withPivot(['value'])->withTimestamps();
    }
}
