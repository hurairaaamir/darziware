<?php

namespace App\Models\Admin\Product;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductAttribute extends Model
{
    use HasFactory;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'item_id', 'description', 'attribute_type', 'data_type', 'unit'
    ];

    public function items()
    {
        return $this->belongsToMany('App\Models\Admin\Item\Item', 'item_product_attributes', 'product_attribute_id', 'item_id');
    }

    public function products()
    {
        return $this->belongsToMany('App\Models\Admin\Product\Product', 'product_product_attributes', 'product_attribute_id', 'product_id')->withPivot(['value'])->withTimestamps();
    }

    public function map_product_attributes()
    {
        return $this->belongsToMany('App\Models\Admin\Product\MapProductAttribute');
    }
}
