<?php

namespace App\Models\Admin\Item;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    use HasFactory;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'gender', 'description', 'dw_item_id', 'dw_item_name','status'
    ];

    public function catalogs()
    {
        return $this->hasMany('App\Models\Admin\Catalog\Catalog');
    }

    public function product_attributes()
    {
        return $this->belongsToMany('App\Models\Admin\Product\ProductAttribute', 'item_product_attributes', 'item_id', 'product_attribute_id');
    }

    public function products()
    {
        return $this->belongsToMany('App\Models\Admin\Product\Product');
    }
}
