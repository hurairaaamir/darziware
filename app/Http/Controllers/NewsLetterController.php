<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\NewsLetter;
use App\Models\Admin\Contact\Contact;

class NewsLetterController extends Controller
{
    public function store(Request $request){
        $referer=explode('/',$request->headers->all()["referer"][0])[2];
        $origin=explode('/',url('/'))[2];
        
        return response()->json([
            "referer"=>$referer,
            "origin"=>$origin
        ]);
        $request = json_decode($request['data']);
        NewsLetter::create([
            "email"=>$request->email
        ]);
        return response()->json([
            "data"=>"NewsLetter Email Stored"
        ]);
    }

    public function index(Request $request){
        
        $filter=$request->filter;
        $searchQuery=$request->searchQuery;
        $newsLetters="";
        $contacts="";
        if($filter=='contact'&&$searchQuery==null){
            $contacts=Contact::all();
        }else if($filter=='email'&&$searchQuery==null){
            $newsLetters=NewsLetter::all();
        }else if($filter=='email'&&$searchQuery!=null){
            $newsLetters=NewsLetter::where('email','LIKE',"%{$searchQuery}%")->get();
        }else if($filter=='contact'&&$searchQuery!=null){
            $contacts=Contact::where('email','LIKE',"%{$searchQuery}%")->get();
        }

        return response()->json([
            "newsLetters"=>$newsLetters,
            "contacts"=>$contacts
        ]);
    }

    public function destroy(Request $request){
        if($request->filter=='email'){
            $data=NewsLetter::whereIn('id',$request->emailIds)->delete();
        }else if($request->filter=='contact'){
            $data=Contact::whereIn('id',$request->emailIds)->delete();
        }
        return response()->json([
            "data"=>$request->all()
        ]);
    }
}
