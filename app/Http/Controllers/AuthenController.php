<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class AuthenController extends Controller
{
    public function login(Request $request)
    {
        $request->validate([
            "email" => "required",
            "password" => "required"
        ]);

        $user = User::where('email', '=', $request->email)->with('role')->first();
        if ($user) {
            $http = new \GuzzleHttp\Client;
            try {
                $response = $http->post($this->getUrl() . '/oauth/token', [

                    'form_params' => [
                        'grant_type' => 'password',
                        'client_id' => '2',
                        'client_secret' => config('services.passport.key'),
                        'username' => $request->email,
                        'password' => $request->password,
                        'scope'  => $user->role->role
                    ]
                ]);

                return $response->getBody();
            } catch (\GuzzleHttp\Exception\BadResponseException $e) {
                if ($e->getCode() === 400) {
                    return response()->json('Invalid Request. Please enter a username or a password.', $e->getCode());
                } else if ($e->getCode() === 401) {
                    return response()->json('Your credentials are incorrect. Please try again', $e->getCode());
                }

                return response()->json('Something went wrong on the server.', $e->getCode());
            }
        } else {
            return response()->json(['message' => 'You are not a user, Kindly Signup first'], 200, $headers);
        }
    }

    public function register(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6',
            'phone_number' => 'required'
        ]);

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'phone_number' => $request->phone_number
        ]);

        $role = Role::create([
            'role' => 'user',
            'user_id' => $user->id
        ]);

        return response()->json([
            "status" => true
        ]);
    }

    public function logout()
    {
        auth()->guard('api')->user()->tokens->each(function ($token, $key) {
            $token->delete();
        });

        return response()->json('Logged out successfully', 200);
    }

    private function getUrl()
    {
        return url("/") == "http://127.0.0.1:8000" ? "http://127.0.0.1:8001" : "https://zainfit.dev.rigrex.com";
    }
}
