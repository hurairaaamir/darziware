<?php

namespace App\Http\Controllers\Admin\Item;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin\Item\Item;
use Illuminate\Support\Facades\Http;
use App\Http\Requests\Admin\Item\ItemRequest;



class ItemController extends Controller
{
    //ADD ITEM
    public function addItem(Request $request)
    {
        if ($request->expectsJson()) {
            $item = new Item($request->item);
            $item->save();


            return response()->json(['id' => $item->id], 200);
        }
    }

    //UPDATE ITEM
    public function updateItem(Request $request, $id)
    {
        if ($request->expectsJson()) {
            $item = Item::find($id);
            $item->update($request->item);
            return response()->json(['item' => $item], 200);
        }
    }

    //REMOVE ITEM
    public function removeItem(Request $request, $id)
    {
        if ($request->expectsJson()) {
            $item = Item::find($id);
            $item->delete();
            return response()->json(['item' => $item], 200);
        }
    }

    //GET ITEMS
    public function getItems(Request $request)
    {
        if ($request->expectsJson()) {
            $items = Item::all();
            return response()->json(['items' => $items], 200);
        }
    }

    //GET DW ITEMS
    public function getDWItems(Request $request)
    {
        // $http = new \GuzzleHttp\Client;

        $response = Http::get(env('ASP_URL').'/app/GetItemlist', [
            'Api_Key' => env('API_KEY')
        ]);

        $items = $response->json()['items'];
        $convertedItems = array();
        $incI = 0;
        foreach($items AS $arrKey => $arrData){
            $convertedItems[$incI]['dw_item_id'] = $arrData['id'];
            $convertedItems[$incI]['dw_item_name'] = $arrData['name'];
            $incI++;
        }

        //Convert array to json form...
        $dwItems = json_encode($convertedItems);
        $dwItems = json_decode($dwItems, true);

        return response()->json(['dwItems' => $dwItems], 200);

    }

    public function statusItem(Request $request){
        foreach($request->all() as $i){
            $status=($i['status']=='active')?"inactive":'active';
            $item=Item::find($i['id']);
            $item->update(["status"=>$status]);
        }
        return response()->json(["data"=>$request->all()]);
    }
}
