<?php

namespace App\Http\Controllers\Admin\Product;

use Illuminate\Http\Request;
use App\Models\Admin\Product\Product;
use App\Models\Admin\Catalog\Catalog;
use App\Models\Admin\Item\Item;
use App\Http\Controllers\Controller;
use Storage;

class ProductController extends Controller
{
    //ADD PRODUCT
    public function addProduct(Request $request)
    {
        if ($request->expectsJson()) {

            $request['data'] = json_decode($request['data']);
            $productAttributes = $request['productAttributes'];
            $singlePictures = $request->singlePictures;
            $imageUrl = $request->imageUrl;
            $mirrorImageUrl = $request->mirrorImageUrl;
            $multiplePictures = $request->multiplePictures;

            $product = new Product();
            $product->title = $request['data']->title;
            $imgPath = Storage::disk('uploads')->put('Products/' . $product->getNextId(), $imageUrl);
            $product->image_url = $imgPath;
            $mirrorImgPath = Storage::disk('uploads')->put('Products/' . $product->getNextId(), $mirrorImageUrl);
            $product->mirror_image_url = $mirrorImgPath;
            $product->on_sale = $request['data']->on_sale == 'true' ? 1 : 0;
            $product->new_product = $request['data']->new_product == 'true' ? 1 : 0;
            $product->offer = $request['data']->offer == 'true' ? 1 : 0;
            $product->discount = $request['data']->discount == 'true' ? 1 : 0;
            $product->old_price = $request['data']->old_price;
            $product->new_price = $request['data']->new_price;
            $product->discount_off = $request['data']->discount_off;
            $product->description = $request['data']->description;
            $product->item_id = $request['data']->item_id;
            $product->item_name = $request['data']->item_name;
            $product->catalog_id = $request['data']->catalog_id;
            $product->catalog_name = $request['data']->catalog_name;
            $product->save();

            foreach ($request['data']->productAttributes as $productAttribute) {
                if ($productAttribute->data_type === 'String') {
                    $product->product_attributes()->attach($productAttribute->attribute_id, ['value' => $productAttribute->value]);
                }
                if ($productAttribute->data_type === 'Number') {
                    $product->product_attributes()->attach($productAttribute->attribute_id, ['value' => $productAttribute->value]);
                }
                if ($productAttribute->data_type === 'Textarea') {
                    $product->product_attributes()->attach($productAttribute->attribute_id, ['value' => $productAttribute->value]);
                }

                if ($productAttribute->data_type === 'Picture') {
                    $imagePath = Storage::disk('uploads')->put('Products/' . $product->id, $singlePictures[$productAttribute->attribute_id]);
                    $product->product_attributes()->attach($productAttribute->attribute_id, ['value' => $imagePath]);
                }

                if ($productAttribute->data_type === 'Multiple Pictures') {
                    $prodMultiplePictures = array();
                    // Getting the length of multiple pictures attribute value
                    for ($i = 0; $i < intval($request['multiplePicturesLength' . $productAttribute->attribute_id]); $i++) {
                        $dynamicFormula = ($i + 1) * ($productAttribute->attribute_id + 1);
                        $picture = $request->multiplePictures[$dynamicFormula];

                        $imagePath = Storage::disk('uploads')->put('Products/' . $product->id, $picture);
                        array_push($prodMultiplePictures, $imagePath);
                    }
                    $productMultiplePictures = implode(",", $prodMultiplePictures);
                    $product->product_attributes()->attach($productAttribute->attribute_id, ['value' => $productMultiplePictures]);
                }
            }

            $addedProduct = Product::with('product_attributes')->where('id', '=', $product->id)->first();
            return response()->json(['id' => $product->id, 'product' => $addedProduct], 200);
        }
    }

    //UPDATE PRODUCT
    public function updateProduct(Request $request, $id)
    {
        if ($request->expectsJson()) {
            $product = Product::find($id);
            $product->update($request->product);
            return response()->json(['product' => $product], 200);
        }
    }

    //REMOVE PRODUCT
    public function removeProduct(Request $request, $id)
    {
        if ($request->expectsJson()) {
            $product = Product::find($id);
            $product->delete();
            return response()->json(['product' => $product], 200);
        }
    }

    //GET PRODUCTS
    public function getProducts(Request $request)
    {
        if ($request->expectsJson()) {
            $products = Product::with('catalog')->get();
            return response()->json(['products' => $products], 200);
        }
    }


    //GET ITEMS
    public function getItems(Request $request)
    {
        if ($request->expectsJson()) {
            $items = Item::select('name', 'id')->where('status','active')->get();
            return response()->json(['items' => $items], 200);
        }
    }

    //GET ITEM CATALOGS
    public function getItemCatalogs(Request $request, $itemId)
    {
        if ($request->expectsJson()) {
            $itemCatalogs = Catalog::select('name', 'id')->where('item_id', '=', $itemId)->get();
            return response()->json(['itemCatalogs' => $itemCatalogs], 200);
        }
    }

    //GET ITEM ATTRIBUTES
    public function getItemAttributes(Request $request, $itemId)
    {
        if ($request->expectsJson()) {
            $itemAttributes = Item::find($itemId)->product_attributes;

            return response()->json(['itemAttributes' => $itemAttributes], 200);
        }
    }

    //GET CATALOGS
    public function getCatalogs(Request $request)
    {
        if ($request->expectsJson()) {
            $catalogs = Catalog::where('status','active')->select('name', 'id')->get();
            return response()->json(['catalogs' => $catalogs], 200);
        }
    }
    // CHANGE STATUS
    public function productStatus(Request $request){
        foreach($request->all() as $i){
            $status=($i['status']=='active')?"inactive":'active';
            $product=Product::find($i['id']);
            $product->update(["status"=>$status]);
        }
        return response()->json(["data"=>$request->all()]);
    }
}
