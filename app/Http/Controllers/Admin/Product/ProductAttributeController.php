<?php

namespace App\Http\Controllers\Admin\Product;

use Illuminate\Http\Request;
use App\Models\Admin\Product\ProductAttribute;
use App\Models\Admin\Item\Item;
use App\Http\Controllers\Controller;


class ProductAttributeController extends Controller
{
    //ADD PRODUCT ATTRIBUTE
    public function addProductAttribute(Request $request)
    {
        if ($request->expectsJson()) {
            $productAttribute = new ProductAttribute($request->productAttribute);
            $productAttribute->save();

            $items = Item::find($request->productAttribute['item_ids']);
            $productAttribute->items()->attach($items);

            $addedProductAttribute = ProductAttribute::with('items')->where('id', '=', $productAttribute->id)->first();
            return response()->json(['id' => $productAttribute->id, 'productAttribute' => $addedProductAttribute], 200);
        }
    }

    //UPDATE PRODUCT ATTRIBUTE
    public function updateProductAttribute(Request $request, $id)
    {
        if ($request->expectsJson()) {
            $productAttribute = ProductAttribute::find($id);
            $productAttribute->update($request->productAttribute);

            $items = Item::find($request->productAttribute['item_ids']);
            $productAttribute->items()->sync($items);

            $updatedProductAttribute = ProductAttribute::with('items')->where('id', '=', $productAttribute->id)->first();

            return response()->json(['productAttribute' => $updatedProductAttribute], 200);
        }
    }

    //REMOVE PRODUCT ATTRIBUTE
    public function removeProductAttribute(Request $request, $id)
    {
        if ($request->expectsJson()) {
            $productAttribute = ProductAttribute::find($id);
            $productAttribute->delete();

            $productAttribute->items()->detach();
            return response()->json(['productAttribute' => $productAttribute], 200);
        }
    }

    //GET PRODUCT ATTRIBUTES
    public function getProductAttributes(Request $request)
    {
        if ($request->expectsJson()) {
            $productAttributes = ProductAttribute::with('items')->get();
            return response()->json(['productAttributes' => $productAttributes], 200);
        }
    }


    //GET ATTRIBUTE ITEMS
    public function getAttributeItems(Request $request)
    {
        if ($request->expectsJson()) {
            $attributeItems = Item::select('name', 'id')->get();
            return response()->json(['attributeItems' => $attributeItems], 200);
        }
    }


}
