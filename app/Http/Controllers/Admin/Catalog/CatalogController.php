<?php

namespace App\Http\Controllers\Admin\Catalog;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin\Catalog\Catalog;
use App\Models\Admin\Item\Item;



class CatalogController extends Controller
{

    //ADD CATALOG
    public function addCatalog(Request $request)
    {
        if ($request->expectsJson()) {
            $catalog = new Catalog($request->catalog);
            $catalog->save();

            $addedCatalog = Catalog::with('item')->where('id', '=', $catalog->id)->first();


            return response()->json(['catalog' => $addedCatalog, 'id' => $catalog->id], 200);
        }
    }

    //UPDATE CATALOG
    public function updateCatalog(Request $request, $id)
    {
        if ($request->expectsJson()) {
            $catalog = Catalog::find($id);
            $catalog->update($request->catalog);
            $updatedCatalog = Catalog::with('item')->where('id', '=', $catalog->id)->first();
            return response()->json(['catalog' => $updatedCatalog], 200);
        }
    }

    //REMOVE CATALOG
    public function removeCatalog(Request $request, $id)
    {
        if ($request->expectsJson()) {
            $catalog = Catalog::find($id);
            $catalog->delete();
            return response()->json(['catalog' => $catalog], 200);
        }
    }

    //GET CATALOGS
    public function getCatalogs(Request $request)
    {
        if ($request->expectsJson()) {
            $catalogs = Catalog::with('item')->get();
            return response()->json(['catalogs' => $catalogs], 200);
        }
    }

    //GET ITEMS
    public function getItems(Request $request)
    {
        if ($request->expectsJson()) {
            $items = Item::select('name', 'id')->get();
            return response()->json(['items' => $items], 200);
        }
    }

    // CHANGE STATUS
    public function catalogStatus(Request $request){
        foreach($request->all() as $i){
            $status=($i['status']=='active')?"inactive":'active';
            $item=Catalog::find($i['id']);
            $item->update(["status"=>$status]);
        }
        return response()->json(["data"=>$request->all()]);
    }
}




// $referer=explode('/',$request->headers->all()["referer"][0]);
// $origin=explode('/',url('/'));

// return response()->json([
//     "referer"=>$referer,
//     "origin"=>$origin
// ]);