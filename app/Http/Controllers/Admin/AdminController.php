<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Hash;
use Session;
use App\Models\Admin\Admin;
use Validator, Input, Redirect;

class AdminController extends Controller
{
    public function logout()
    {
        Auth::guard('admin')->logout();
        return redirect('/admin/login');
    }


    public function updateProfile(Request $request)
    {

        // validate rules
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255',
            'password' => 'required|min:6'
        ]);

        // what to do if validator fails
        if ($validator->fails()) {

            return redirect()->back()->withErrors($validator)->withInput();
        } else {

            // find the Logged In Admin
            $admin = Admin::find(Auth::user()->id);
            $password = $request->password;
            $name = $request->name;
            $email = $request->email;

            $hashed_password = $admin->password;

            // checking the old pass with new one
            if (Hash::check($password, $hashed_password)) {

                $admin->update([
                    'name' => $name,
                    'email' => $email

                ]);

                return redirect()->back()->with('success', 'Changes Saved Successfully');
            } else {

                return redirect()->back()->with('error', 'Your Password is Wrong!');
            }
        }
    }

    public function updatePassword(Request $request)
    {

        // validate rules
        $validator = Validator::make($request->all(), [
            'old_password' => 'required|min:6',
            'password' => 'required|confirmed|min:6',
        ]);

        // what to do if validator fails
        if ($validator->fails()) {

            return redirect()->back()->withErrors($validator)->withInput();
        } else {

            // find the Logged In Admin
            $admin = Admin::find(Auth::user()->id);
            $old_password = $request->old_password;
            $new_password = $request->password;

            $hashed_password = $admin->password;

            // checking the old pass with entered one
            if (Hash::check($old_password, $hashed_password)) {

                $admin->update([
                    'password' => Hash::make($new_password)
                ]);

                return redirect()->back()->with('success', 'Password Changed Successfully');
            } else {

                return redirect()->back()->with('error', 'You do not have credentials to change Password!');
            }
        }
    }
}
