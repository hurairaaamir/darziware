<?php

namespace App\Http\Controllers\Api\Catalog;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin\Catalog\Catalog;


class CatalogController extends Controller
{
    public function getCatalogs()
    {
        $catalogs = Catalog::where('status','active')->get();
        return response()->json(['catalogs' => $catalogs], 200);
    }
}
