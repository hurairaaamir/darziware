<?php

namespace App\Http\Controllers\Api\Item;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin\Item\Item;


class ItemController extends Controller
{
    //GET ITEMS
    public function getItems(Request $request)
    {
        $items = Item::where('status','active')->with('catalogs')->get();
        return response()->json(['items' => $items], 200);
    }

    //GET MEN ITEMS
    public function getMenItems(Request $request)
    {
        $items = Item::where('status','active')->with('catalogs')->where('gender', '=', 'Male')->get();
        return response()->json(['items' => $items], 200);
    }


    //GET WOMEN ITEMS
    public function getWomenItems(Request $request)
    {
        $items = Item::where('status','active')->with('catalogs')->where('gender', '=', 'Female')->get();
        return response()->json(['items' => $items], 200);
    }
}
