<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin\Contact\Contact;


class ApiController extends Controller
{
    public function contactUs(Request $request)
    {
        $request['data'] = json_decode($request['data']);

        $contact = new Contact();
        $contact->name = $request['data']->name;
        $contact->email = $request['data']->email;
        $contact->purpose = $request['data']->purpose;
        if ($request['data']->orderNo !== '') {
            $contact->orderNo = $request['data']->orderNo;
        }
        $contact->message = $request['data']->message;
        $contact->save();
        return response()->json(['message' => 'Your Response is Successfully Submitted'], 200);
    }

    
}
