<?php

namespace App\Http\Controllers\Api\Product;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin\Item\Item;
use App\Models\Admin\Product\Product;


class ProductController extends Controller
{
    //GET PRODUCTS
    public function getProducts(Request $request)
    {
        $products = Product::where('status', 'active')->with('product_attributes')->get();
        return response()->json(['products' => $products], 200);
    }

    //GET PRODUCT
    public function getProduct(Request $request, $id)
    {
        $product = Product::where('status', 'active')->with('product_attributes')->where('id', '=', $id)->first();
        $dw_item_id = Item::where('id', '=', $product->item_id)->first()->dw_item_id;
        return response()->json(['product' => $product, 'dw_item_id' => $dw_item_id], 200);
    }

    //GET CATALOG PRODUCTS
    public function getCatalogProducts(Request $request, $catalog_id)
    {

        $catalog_products = Product::where('status', 'active')->with('product_attributes')->where('catalog_id', '=', $catalog_id)->get();
        return response()->json(['catalog_products' => $catalog_products], 200);
    }

    //GET SEARCHED PRODUCTS
    public function getSearchProducts(Request $request, $search)
    {
        $search_products = Product::search($search)->with('product_attributes')->get();
        return response()->json(['search_products' => $search_products], 200);
    }
}
