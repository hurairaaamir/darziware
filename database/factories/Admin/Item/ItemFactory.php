<?php

namespace Database\Factories\Admin\Item;

use App\Models\Admin\Item\Item;
use Illuminate\Database\Eloquent\Factories\Factory;

class ItemFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Item::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->randomElement(array('Shirt', 'Trouser', 'Coat', 'Jacket', 'Blazer', 'Thobe', 'Kurta')),
            'gender' => $this->faker->randomElement(array('Male', 'Female')),
            'description' => $this->faker->paragraph,
            'dw_item_name' => $this->faker->randomElement(array('Jacket', 'Shirt', 'Trouser', 'Waist Coat', '2 pcs Suit', '3 pcs Suit', 'Jacket Alteration', 'COAT ALTERATION', 'COAT STITCHING')),
            'dw_item_id' => $this->faker->randomElement(array(1381, 1380, 1381, 1383, 1384, 1385, 1400, 1402, 1403)),
        ];
    }
}
