<?php

namespace Database\Factories\Admin\Catalog;

use App\Models\Admin\Item\Item;
use App\Models\Admin\Catalog\Catalog;
use Illuminate\Database\Eloquent\Factories\Factory;

class CatalogFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Catalog::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->randomElement($array = array('Casual Shirt', 'Casual Trouser', 'Casual Coat', 'Casual Jacket', 'Casual Blazer', 'Casual Thobe', 'Casual Kurta')),
            'description' => $this->faker->paragraph,
            'item_id' => Item::factory(),
        ];
    }
}