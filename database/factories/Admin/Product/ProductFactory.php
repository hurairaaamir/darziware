<?php

namespace Database\Factories\Admin\Product;

use App\Models\Admin\Product\Product;
use App\Models\Admin\Item\Item;
use App\Models\Admin\Catalog\Catalog;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => $this->faker->randomElement(array('Casual Shirt', 'Casual Pant', 'Casual Trouser', 'Casual Thobe', 'Casual Coat', 'Dress Shirt', 'Dress Pant', 'Dress Trouser', 'Dress Thobe', 'Dress Coat')),
            'image_url' => $this->faker->randomElement(array('Products/1/ajmwDfYgK4qt3JuYfFcavx22dl75ILnpEsUqBpjm.jpeg', 'Products/2/MI6kHZgr7gtnljjq4F0I8jzY0UlufxBQrWJ4fN1G.jpeg')),
            'mirror_image_url' => $this->faker->randomElement(array('Products/1/YHo0C0yCJauuqG7yQMZGbi0P0bHN2Z5H4yOSIlLF.jpeg', 'Products/2/pnXCgMZa4Fy6rxVRF11z4vvqWY40jRKpDo4wOUAF.jpeg')),
            'on_sale' => $this->faker->randomElement(array(1, 0)),
            'new_product' => $this->faker->randomElement(array(1, 0)),
            'offer' => $this->faker->randomElement(array(1, 0)),
            'discount' => $this->faker->randomElement(array(1, 0)),
            'old_price' => $this->faker->randomElement(array('400', '500', '200', '300', '100', '600', '700', '800', '900', '1000', '1100', '12000')),
            'new_price' => $this->faker->randomElement(array('400', '500', '200', '300', '100', '600', '700', '800', '900', '1000', '1100', '12000')),
            'discount_off' => $this->faker->randomElement(array('2', '10', '5', '15', '20', '25')),
            'description' => $this->faker->paragraph(3),
            'catalog_id' => $this->faker->randomElement(array(1, 2, 3, 4, 5)),
            'catalog_name' => $this->faker->randomElement(array('Shirt', 'Pant', 'Trouser', 'Thobe', 'Coat')),
            'item_id' => $this->faker->randomElement(array(1, 2, 3, 4, 5, 6, 7)),
            'item_name' => $this->faker->randomElement(array('Shirt', 'Pant', 'Trouser', 'Thobe', 'Coat')),
        ];
    }
}
