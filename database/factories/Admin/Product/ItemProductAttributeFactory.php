<?php

namespace Database\Factories\Admin\Product;

use App\Models\Admin\Product\ItemProductAttribute;
use App\Models\Admin\Product\ProductAttribute;
use App\Models\Admin\Item\Item;
use Illuminate\Database\Eloquent\Factories\Factory;

class ItemProductAttributeFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ItemProductAttribute::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'product_attribute_id' => ProductAttribute::factory(),
            'item_id' => Item::factory(),
        ];
    }
}
