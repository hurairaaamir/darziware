<?php

namespace Database\Factories\Admin\Product;

use App\Models\Admin\Product\ProductAttribute;
use App\Models\Admin\Item\Item;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class ProductAttributeFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ProductAttribute::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->randomElement($array = array('Length', 'Weight', 'Size', 'Colour', 'Brand', 'Design Number', 'Catalog ID', 'Rate', 'Offer Price', 'Discount')),
            'description' => $this->faker->paragraph,
            'attribute_type' => $this->faker->randomElement($array = array('P', 'S')),
            'data_type' => $this->faker->randomElement($array = array('Number', 'Text', 'Boolean', 'String')),
            'unit' => $this->faker->randomElement($array = array('Meters', 'Kg', 'Inches', 'Grams', 'NA')),
        ];
    }
}
