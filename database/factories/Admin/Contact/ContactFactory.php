<?php

namespace Database\Factories\Admin\Contact;

use App\Models\Admin\Contact\Contact;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class ContactFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Contact::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name,
            'email' => $this->faker->unique()->safeEmail,
            'purpose' => $this->faker->randomElement(["Feedback","Complain"]),
            'orderNo' => $this->faker->numberBetween(1000,9000),
            'message' => $this->faker->paragraph(3),
        ];
    }
}
