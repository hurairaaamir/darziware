<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('image_url');
            $table->string('mirror_image_url');
            $table->boolean('on_sale')->default(false);
            $table->boolean('new_product')->default(false);
            $table->boolean('offer')->default(false);
            $table->boolean('discount')->default(false);
            $table->string('old_price');
            $table->string('new_price')->nullable();
            $table->string('discount_off')->nullable();
            $table->longText('description');
            $table->unsignedBigInteger('catalog_id');
            $table->string('catalog_name');
            $table->unsignedBigInteger('item_id');
            $table->string('item_name');
            $table->enum("status",['active','inactive'])->default('active');
            $table->foreign('catalog_id')
                ->references('id')->on('catalogs')
                ->onDelete('cascade');
            $table->foreign('item_id')
                ->references('id')->on('items')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
