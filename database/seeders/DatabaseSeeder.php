<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Admin\Product\ItemProductAttribute;
use App\Models\Admin\Catalog\Catalog;
use App\Models\Admin\Product\Product;
use App\Models\Admin\Contact\Contact;
use App\Models\NewsLetter;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        DB::table('oauth_clients')->insert([
            "id" => 1,
            "user_id" => null,
            'name' => "Laravel Personal Access Client",
            "secret" => "15ZB7Uz2aSROflGgPp7XPhAfsFwtZGVLcrc3RZ9i",
            "provider" => null,
            "redirect" => "http://localhost",
            "personal_access_client" => 1,
            "password_client" => 0,
            "revoked" => 0
        ]);

        DB::table('oauth_clients')->insert([
            "id" => 2,
            "user_id" => null,
            'name' => "Laravel Password Grant Client",
            "secret" => "fmTKwSaSFycytDgB5F6ntiS0wcWxIRpuSrLX3I5A",
            "provider" => "users",
            "redirect" => "http://localhost",
            "personal_access_client" => 0,
            "password_client" => 1,
            "revoked" => 0
        ]);

        DB::table('oauth_personal_access_clients')->insert([
            "id" => 1,
            "client_id" => 1
        ]);





        ItemProductAttribute::factory(4)->create();
        Catalog::factory(5)->create();
        Product::factory(38)->create();
        Contact::factory(40)->create();
        NewsLetter::factory(30)->create();

        $this->call(UserSeeder::class);
        $this->call(RoleSeeder::class);

    }
}

