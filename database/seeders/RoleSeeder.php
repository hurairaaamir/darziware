<?php

namespace Database\Seeders;


use Illuminate\Database\Seeder;
use App\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create([
            'user_id' => 1,
            'role' => 'admin'
        ]);

        Role::create([
            'user_id' => 2,
            'role' => 'user'
        ]);
    }
}
