<?php

namespace Database\Seeders;


use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\NewsLetter;
use App\Models\Admin\Contact;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        User::create([
            'name' => "admin",
            'email' => "admin@admin.com",
            'email_verified_at' => now(),
            'phone_number' => '+923087877879',
            'password' => Hash::make('password'), // password
        ]);

        User::create([
            'name' => "User",
            'email' => "user@user.com",
            'email_verified_at' => now(),
            'phone_number' => '+923087877879',
            'password' => Hash::make('password'), // password
        ]);
    }
}
