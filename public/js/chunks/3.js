(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[3],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/email/ContactItem.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/pages/email/ContactItem.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  props: {
    contact: {
      type: Object,
      required: true
    },
    isSelected: {
      type: Boolean,
      required: true
    }
  },
  data: function data() {
    return {
      isSelectedMail: this.isSelected
    };
  },
  watch: {
    isSelected: function isSelected(value) {
      this.isSelectedMail = value;
    },
    isSelectedMail: function isSelectedMail(val) {
      val ? this.$emit('addToSelected', this.contact.id) : this.$emit('removeSelected', this.contact.id);
    }
  },
  methods: {}
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/email/Email.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/pages/email/Email.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _EmailSidebar_vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./EmailSidebar.vue */ "./resources/js/src/views/pages/email/EmailSidebar.vue");
/* harmony import */ var _ContactItem_vue__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ContactItem.vue */ "./resources/js/src/views/pages/email/ContactItem.vue");
/* harmony import */ var _NewsLetterItem_vue__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./NewsLetterItem.vue */ "./resources/js/src/views/pages/email/NewsLetterItem.vue");
/* harmony import */ var vue_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! vue-perfect-scrollbar */ "./node_modules/vue-perfect-scrollbar/dist/index.js");
/* harmony import */ var vue_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(vue_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _store_email_moduleEmail_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @/store/email/moduleEmail.js */ "./resources/js/src/store/email/moduleEmail.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//





/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      openMailId: null,
      selectedMails: [],
      isSidebarActive: false,
      showThread: false,
      clickNotClose: true,
      isEmailSidebarActive: true,
      settings: {
        maxScrollbarLength: 60,
        wheelSpeed: 0.30
      },
      searchQuery: ''
    };
  },
  watch: {
    isSidebarActive: function isSidebarActive(value) {
      if (!value) this.showThread = false;
    },
    emailSearch: function emailSearch() {
      this.selectedMails = [];
      this.$refs.mailListPS.$el.scrollTop = 0;
      this.$store.dispatch("email/fetchEmails", {
        filter: this.$route.params.filter,
        searchQuery: this.searchQuery
      });
      this.toggleEmailSidebar();
    },
    windowWidth: function windowWidth() {
      this.setSidebarWidth();
    }
  },
  computed: {
    mailFilter: function mailFilter() {
      return this.$route.params.filter;
    },
    selectAllCheckBox: {
      get: function get() {
        return this.selectedMails.length;
      },
      set: function set(value) {
        if (this.$route.params.filter == 'contact') {
          value ? this.selectedMails = this.contacts.map(function (mail) {
            return mail.id;
          }) : this.selectedMails = [];
        } else if (this.$route.params.filter == 'email') {
          value ? this.selectedMails = this.newsLetters.map(function (mail) {
            return mail.id;
          }) : this.selectedMails = [];
        }
      }
    },
    contacts: function contacts() {
      return this.$store.state.email.contacts;
    },
    newsLetters: function newsLetters() {
      return this.$store.state.email.newsLetters;
    },
    selectAllIcon: function selectAllIcon() {
      return this.selectedMails.length == this.contacts.length ? 'icon-check' : 'icon-minus';
    },
    isMailSelected: function isMailSelected() {
      var _this = this;

      return function (mailId) {
        return _this.selectedMails.indexOf(mailId) == -1 ? false : true;
      };
    },
    windowWidth: function windowWidth() {
      return this.$store.state.windowWidth;
    }
  },
  methods: {
    addToSelectedMails: function addToSelectedMails(mailId) {
      if (this.selectedMails.indexOf(mailId) === -1) this.selectedMails.push(mailId);
      console.log(this.selectedMails);
    },
    removeSelectedMail: function removeSelectedMail(mailId) {
      var mailIndex = this.selectedMails.indexOf(mailId);
      if (mailIndex !== -1) this.selectedMails.splice(mailIndex, 1);
    },
    deleteRecord: function deleteRecord() {
      if (confirm("Are you sure !!?")) {
        var payload = {
          emailIds: this.selectedMails,
          filter: this.$route.params.filter
        };
        this.$store.dispatch('email/deleteRecord', payload).then(function (response) {}).catch(function (error) {
          console.error(error);
        });
        this.selectedMails = [];
      }
    },
    closeMailViewSidebar: function closeMailViewSidebar() {
      this.isSidebarActive = false;
    },
    setSidebarWidth: function setSidebarWidth() {
      if (this.windowWidth < 992) {
        this.isEmailSidebarActive = this.clickNotClose = false;
      } else {
        this.isEmailSidebarActive = this.clickNotClose = true;
      }
    },
    toggleEmailSidebar: function toggleEmailSidebar() {
      var value = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;

      if (!value) {
        this.closeMailViewSidebar();

        if (this.clickNotClose) {
          return;
        }
      }

      this.isEmailSidebarActive = value;
    },
    emailSearch: function emailSearch() {
      this.$store.dispatch("email/fetchEmails", {
        filter: this.$route.params.filter,
        searchQuery: this.searchQuery
      }); // Fetch Emails From API
    }
  },
  components: {
    ContactItem: _ContactItem_vue__WEBPACK_IMPORTED_MODULE_1__["default"],
    EmailSidebar: _EmailSidebar_vue__WEBPACK_IMPORTED_MODULE_0__["default"],
    VuePerfectScrollbar: vue_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_3___default.a,
    NewsLetterItem: _NewsLetterItem_vue__WEBPACK_IMPORTED_MODULE_2__["default"]
  },
  created: function created() {
    this.$store.registerModule('email', _store_email_moduleEmail_js__WEBPACK_IMPORTED_MODULE_4__["default"]);
    this.setSidebarWidth();
    this.$store.dispatch("email/fetchEmails", {
      filter: this.$route.params.filter,
      searchQuery: this.searchQuery
    }); // Fetch Emails From API
  },
  beforeDestroy: function beforeDestroy() {
    this.$store.unregisterModule('email');
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/email/EmailSidebar.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/pages/email/EmailSidebar.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _store_email_moduleEmail_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @/store/email/moduleEmail.js */ "./resources/js/src/store/email/moduleEmail.js");
/* harmony import */ var quill_dist_quill_core_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! quill/dist/quill.core.css */ "./node_modules/quill/dist/quill.core.css");
/* harmony import */ var quill_dist_quill_core_css__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(quill_dist_quill_core_css__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var quill_dist_quill_snow_css__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! quill/dist/quill.snow.css */ "./node_modules/quill/dist/quill.snow.css");
/* harmony import */ var quill_dist_quill_snow_css__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(quill_dist_quill_snow_css__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var vue_quill_editor__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! vue-quill-editor */ "./node_modules/vue-quill-editor/dist/vue-quill-editor.js");
/* harmony import */ var vue_quill_editor__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(vue_quill_editor__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var vue_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! vue-perfect-scrollbar */ "./node_modules/vue-perfect-scrollbar/dist/index.js");
/* harmony import */ var vue_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(vue_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_4__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//





/* harmony default export */ __webpack_exports__["default"] = ({
  props: {
    mailFilter: {
      type: String
    }
  },
  data: function data() {
    return {
      activePrompt: false,
      mailTo: '',
      mailSubject: '',
      mailCC: '',
      mailBCC: '',
      mailMessage: '',
      editorOption: {
        modules: {
          toolbar: [['bold', 'italic', 'underline', 'strike', 'link', 'blockquote', 'code-block'], [{
            'header': 1
          }, {
            'header': 2
          }], [{
            'list': 'ordered'
          }, {
            'list': 'bullet'
          }], [{
            'font': []
          }]]
        },
        placeholder: 'Message'
      },
      settings: {
        maxScrollbarLength: 60,
        wheelSpeed: 0.30
      }
    };
  },
  computed: {
    validateForm: function validateForm() {
      return !this.errors.any() && this.mailTo != '';
    },
    contacts: function contacts() {
      return this.$store.state.email.contacts;
    },
    newsLetters: function newsLetters() {
      return this.$store.state.email.newsLetters;
    },
    baseUrl: function baseUrl() {
      var path = this.$route.path;
      return path.slice(0, path.lastIndexOf("/"));
    },
    emailMeta: function emailMeta() {
      return this.$store.state.email.meta;
    }
  },
  methods: {
    clearFields: function clearFields() {
      var _this = this;

      this.$nextTick(function () {
        _this.mailTo = '';
        _this.mailSubject = '';
        _this.mailCC = '';
        _this.mailBCC = '';
        _this.mailMessage = '';
      });
    },
    sendMail: function sendMail() {
      this.clearFields();
    }
  },
  components: {
    quillEditor: vue_quill_editor__WEBPACK_IMPORTED_MODULE_3__["quillEditor"],
    VuePerfectScrollbar: vue_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_4___default.a
  },
  created: function created() {
    this.$store.registerModule('email', _store_email_moduleEmail_js__WEBPACK_IMPORTED_MODULE_0__["default"]);
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/email/NewsLetterItem.vue?vue&type=script&lang=js&":
/*!************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/pages/email/NewsLetterItem.vue?vue&type=script&lang=js& ***!
  \************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  props: {
    newsLetter: {
      type: Object,
      required: true
    },
    isSelected: {
      type: Boolean,
      required: true
    }
  },
  data: function data() {
    return {
      isSelectedMail: this.isSelected
    };
  },
  watch: {
    isSelected: function isSelected(value) {
      this.isSelectedMail = value;
    },
    isSelectedMail: function isSelectedMail(val) {
      val ? this.$emit('addToSelected', this.newsLetter.id) : this.$emit('removeSelected', this.newsLetter.id);
    }
  },
  methods: {
    getEmail: function getEmail(email) {
      var date = email.split('T')[0];
      var time = email.split('T')[1].split('.')[0];
      return date + "   " + time;
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/email/Email.vue?vue&type=style&index=0&lang=scss&":
/*!**************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--8-2!./node_modules/sass-loader/dist/cjs.js??ref--8-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/pages/email/Email.vue?vue&type=style&index=0&lang=scss& ***!
  \**************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/*=========================================================================================\n    File Name: email.scss\n    Description: Email app's styles. This is imported in Email.vue file\n    ----------------------------------------------------------------------------------------\n    Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template\n      Author: Pixinvent\n    Author URL: http://www.themeforest.net/user/pixinvent\n==========================================================================================*/\n#email-app .vs-sidebar--background {\n  position: absolute;\n}\n[dir=ltr] #email-app .select-all-chexkbox .vs-checkbox {\n  margin-right: 0.8rem;\n}\n[dir=rtl] #email-app .select-all-chexkbox .vs-checkbox {\n  margin-left: 0.8rem;\n}\n#email-app .email-filter-scroll-area {\n  position: relative;\n  width: 100%;\n  height: calc(100% - 75px);\n}\n[dir] #email-app .email-filter-scroll-area {\n  margin: auto;\n}\n#email-app .email-content-scroll-area {\n  position: relative;\n  width: 100%;\n  height: calc(100% - 100px);\n}\n[dir] #email-app .email-content-scroll-area {\n  margin: auto;\n}\n#email-app .scroll-area-mail-content {\n  position: relative;\n  width: 100%;\n  height: calc(100% - 6.3%);\n}\n[dir] #email-app .scroll-area-mail-content {\n  margin: auto;\n}\n[dir] #email-app .email__mails .mail__opened-mail {\n  background-color: #eee;\n}\n#email-app .email__mails .email__mail-item {\n  transition: all 0.35s;\n}\n[dir] #email-app .email__mails .email__mail-item:not(:first-of-type) .mail__mail-item {\n  border-top: 1px solid #dae1e7;\n}\n#email-app .email__mails .email__mail-item .mail__mail-item {\n  transition: all 0.2s;\n}\n#email-app .email__mails .email__mail-item .mail__mail-item:hover {\n  transition: all 0.2s;\n}\n[dir] #email-app .email__mails .email__mail-item .mail__mail-item:hover {\n  transform: translateY(-4px);\n  box-shadow: 0px 3px 10px 0px #ccc;\n}\n#email-app .email__mails .email__mail-item .mail__mail-item .feather-icon {\n  color: #9c9c9c;\n}\n[dir] #email-app .email-view-sidebar .vs-sidebar {\n  background-color: #f8f8f8 !important;\n}\n[dir=ltr] #email-app .email-view-sidebar .vs-sidebar {\n  border-left: 1px solid #dae1e7;\n}\n[dir=rtl] #email-app .email-view-sidebar .vs-sidebar {\n  border-right: 1px solid #dae1e7;\n}\n#email-app .email-view-sidebar .email-header {\n  z-index: 1;\n  position: relative;\n}\n[dir] #email-app .email-view-sidebar .email-header {\n  box-shadow: 0px 4px 6px rgba(0, 0, 0, 0.04);\n  border-bottom: 1px solid #eee;\n}\n#email-app .email-view-sidebar .open-mail-label {\n  transition: all 0.35s;\n}\n[dir=ltr] #email-app .email-view-sidebar .mail__attachment .vs-chip--text {\n  margin-left: 0;\n  margin-right: 0;\n}\n[dir=rtl] #email-app .email-view-sidebar .mail__attachment .vs-chip--text {\n  margin-right: 0;\n  margin-left: 0;\n}\n.con-vs-dialog.email-compose .vs-dialog {\n  max-width: 530px;\n}\n[dir] .con-vs-dialog.email-compose .vs-dialog .vs-dialog-text {\n  padding: 0;\n}\n.con-vs-dialog.email-compose .vs-dialog .vs-dialog-text .scroll-area {\n  max-height: 75vh;\n}\n[dir] .con-vs-dialog.email-compose .vs-dialog .vs-dialog-text .compose-mail-attachment .con-input-upload {\n  margin: 0;\n}\n.con-vs-dialog.email-compose .con-img-upload .con-input-upload {\n  height: 2.5rem;\n  width: 100%;\n}\n[dir] .con-vs-dialog.email-compose .con-img-upload .img-upload {\n  margin: 0;\n  margin-bottom: 0.5rem;\n}\n[dir=ltr] .con-vs-dialog.email-compose .con-img-upload .img-upload {\n  margin-right: 1.5rem;\n}\n[dir=rtl] .con-vs-dialog.email-compose .con-img-upload .img-upload {\n  margin-left: 1.5rem;\n}\n[dir] .con-vs-dialog.email-compose .quill-editor {\n  margin-bottom: 1.5rem;\n}\n.con-vs-dialog.email-compose .ql-editor {\n  height: 150px;\n}", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/email/Email.vue?vue&type=style&index=0&lang=scss&":
/*!******************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--8-2!./node_modules/sass-loader/dist/cjs.js??ref--8-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/pages/email/Email.vue?vue&type=style&index=0&lang=scss& ***!
  \******************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../../../node_modules/css-loader!../../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../../node_modules/postcss-loader/src??ref--8-2!../../../../../../node_modules/sass-loader/dist/cjs.js??ref--8-3!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Email.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/email/Email.vue?vue&type=style&index=0&lang=scss&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/email/ContactItem.vue?vue&type=template&id=7226951b&":
/*!*************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/pages/email/ContactItem.vue?vue&type=template&id=7226951b& ***!
  \*************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "mail__mail-item sm:px-4 px-2 py-6 " }, [
    _c("div", { staticClass: "flex w-full" }, [
      _c("div", { staticClass: "flex w-full justify-between items-start" }, [
        _c("div", { staticClass: "mail__details" }, [
          _c("h5", { staticClass: "mb-1 font-semibold" }, [
            _vm._v(_vm._s(_vm.contact.name))
          ]),
          _vm._v(" "),
          _c("span", [_vm._v(_vm._s(_vm.contact.email))]),
          _vm._v(" "),
          _vm.contact.purpose != "Feedback"
            ? _c(
                "span",
                {
                  staticStyle: {
                    float: "right",
                    position: "absolute",
                    right: "0px",
                    "margin-right": "15px"
                  }
                },
                [_vm._v(_vm._s(_vm.contact.orderNo))]
              )
            : _vm._e()
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "mail-item__meta flex items-center" }, [
          _c(
            "div",
            { staticClass: "email__labels hidden sm:flex items-center" },
            [
              _c("div", {
                staticClass: "h-2 w-2 rounded-full mr-2",
                class:
                  _vm.contact.purpose == "Feedback" ? "bg-primary" : "bg-danger"
              })
            ]
          ),
          _vm._v(" "),
          _c("span", [_vm._v(_vm._s(_vm.contact.purpose))])
        ])
      ])
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "flex w-full" }, [
      _c(
        "div",
        { staticClass: "flex items-center ml-1" },
        [
          _c("vs-checkbox", {
            staticClass: "vs-checkbox-small ml-0 mr-1",
            on: {
              click: function($event) {
                $event.stopPropagation()
              }
            },
            model: {
              value: _vm.isSelectedMail,
              callback: function($$v) {
                _vm.isSelectedMail = $$v
              },
              expression: "isSelectedMail"
            }
          })
        ],
        1
      ),
      _vm._v(" "),
      _c("div", { staticClass: "mail__message  ml-3" }, [
        _c("span", [_vm._v(_vm._s(_vm.contact.message))])
      ])
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/email/Email.vue?vue&type=template&id=5aae5638&":
/*!*******************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/pages/email/Email.vue?vue&type=template&id=5aae5638& ***!
  \*******************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    {
      staticClass:
        "border border-solid d-theme-border-grey-light rounded relative overflow-hidden",
      attrs: { id: "email-app" }
    },
    [
      _c(
        "vs-sidebar",
        {
          staticClass: "items-no-padding",
          attrs: {
            parent: "#email-app",
            "click-not-close": _vm.clickNotClose,
            "hidden-background": _vm.clickNotClose
          },
          model: {
            value: _vm.isEmailSidebarActive,
            callback: function($$v) {
              _vm.isEmailSidebarActive = $$v
            },
            expression: "isEmailSidebarActive"
          }
        },
        [
          _c("email-sidebar", {
            attrs: { mailFilter: _vm.mailFilter },
            on: { closeSidebar: _vm.toggleEmailSidebar }
          })
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "div",
        {
          staticClass:
            "no-scroll-content border border-solid d-theme-border-grey-light border-r-0 border-t-0 border-b-0",
          class: { "sidebar-spacer": _vm.clickNotClose }
        },
        [
          _c(
            "div",
            { staticClass: "flex border d-theme-dark-bg items-center" },
            [
              _c("feather-icon", {
                staticClass:
                  "md:inline-flex lg:hidden ml-4 mr-4 cursor-pointer",
                attrs: { icon: "MenuIcon" },
                on: {
                  click: function($event) {
                    $event.stopPropagation()
                    return _vm.toggleEmailSidebar(true)
                  }
                }
              }),
              _vm._v(" "),
              _c("vs-input", {
                staticClass:
                  "vs-input-no-border vs-input-no-shdow-focus w-full",
                attrs: {
                  "icon-no-border": "",
                  icon: "icon-search",
                  size: "large",
                  "icon-pack": "feather",
                  placeholder: "Search Mail"
                },
                on: { keyup: _vm.emailSearch },
                model: {
                  value: _vm.searchQuery,
                  callback: function($$v) {
                    _vm.searchQuery = $$v
                  },
                  expression: "searchQuery"
                }
              })
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "div",
            {
              staticClass:
                "email__actions flex items-center flex-wrap justify-between p-4 border border-r-0 border-l-0 border-solid d-theme-border-grey-light"
            },
            [
              _c(
                "div",
                { staticClass: "flex items-center" },
                [
                  _c(
                    "vs-checkbox",
                    {
                      staticClass: "select-all-chexkbox ml-0",
                      attrs: {
                        "icon-pack": "feather",
                        icon: _vm.selectAllIcon
                      },
                      model: {
                        value: _vm.selectAllCheckBox,
                        callback: function($$v) {
                          _vm.selectAllCheckBox = $$v
                        },
                        expression: "selectAllCheckBox"
                      }
                    },
                    [_vm._v("Select All")]
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "flex" },
                [
                  _c("feather-icon", {
                    staticClass: "cursor-pointer ml-5",
                    attrs: { icon: "TrashIcon", "svg-classes": "h-6 w-6" },
                    on: {
                      click: function($event) {
                        return _vm.deleteRecord()
                      }
                    }
                  })
                ],
                1
              )
            ]
          ),
          _vm._v(" "),
          _c(
            "VuePerfectScrollbar",
            {
              ref: "mailListPS",
              staticClass: "email-content-scroll-area",
              attrs: { settings: _vm.settings }
            },
            [
              _c(
                "transition-group",
                {
                  directives: [
                    {
                      name: "show",
                      rawName: "v-show",
                      value: _vm.$route.params.filter == "contact",
                      expression: "$route.params.filter=='contact'"
                    }
                  ],
                  staticClass: "email__mails",
                  attrs: { name: "list-enter-up", tag: "ul", appear: "" }
                },
                _vm._l(_vm.contacts, function(contact) {
                  return _c(
                    "li",
                    {
                      key: "contact_" + contact.id,
                      staticClass: "cursor-pointer email__mail-item",
                      on: {
                        click: function($event) {
                          return _vm.updateOpenMail(contact.id)
                        }
                      }
                    },
                    [
                      _c("ContactItem", {
                        attrs: {
                          contact: contact,
                          isSelected: _vm.isMailSelected(contact.id)
                        },
                        on: {
                          addToSelected: _vm.addToSelectedMails,
                          removeSelected: _vm.removeSelectedMail
                        }
                      })
                    ],
                    1
                  )
                }),
                0
              ),
              _vm._v(" "),
              _c(
                "transition-group",
                {
                  directives: [
                    {
                      name: "show",
                      rawName: "v-show",
                      value: _vm.$route.params.filter == "email",
                      expression: "$route.params.filter=='email'"
                    }
                  ],
                  staticClass: "email__mails",
                  attrs: { name: "list-enter-up", tag: "ul", appear: "" }
                },
                _vm._l(_vm.newsLetters, function(newsLetter) {
                  return _c(
                    "li",
                    {
                      key: "newsLetter_" + newsLetter.id,
                      staticClass: "cursor-pointer email__mail-item",
                      on: {
                        click: function($event) {
                          return _vm.updateOpenMail(newsLetter.id)
                        }
                      }
                    },
                    [
                      _c("NewsLetterItem", {
                        attrs: {
                          newsLetter: newsLetter,
                          isSelected: _vm.isMailSelected(newsLetter.id)
                        },
                        on: {
                          addToSelected: _vm.addToSelectedMails,
                          removeSelected: _vm.removeSelectedMail
                        }
                      })
                    ],
                    1
                  )
                }),
                0
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/email/EmailSidebar.vue?vue&type=template&id=e24b6ff0&":
/*!**************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/pages/email/EmailSidebar.vue?vue&type=template&id=e24b6ff0& ***!
  \**************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "email__email-sidebar h-full" },
    [
      _c(
        "div",
        { staticClass: "m-6 clearfix" },
        [
          _c(
            "vs-button",
            {
              staticClass: "bg-primary-gradient w-full",
              attrs: { "icon-pack": "feather", icon: "icon-edit" },
              on: {
                click: function($event) {
                  _vm.activePrompt = true
                }
              }
            },
            [_vm._v("Compose")]
          )
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "vs-prompt",
        {
          staticClass: "email-compose",
          attrs: {
            title: "New Message",
            "accept-text": "Send",
            "is-valid": _vm.validateForm,
            active: _vm.activePrompt
          },
          on: {
            cancel: _vm.clearFields,
            accept: _vm.sendMail,
            close: _vm.clearFields,
            "update:active": function($event) {
              _vm.activePrompt = $event
            }
          }
        },
        [
          _c(
            "VuePerfectScrollbar",
            {
              key: _vm.$vs.rtl,
              staticClass: "scroll-area p-4",
              attrs: { settings: _vm.settings }
            },
            [
              _c(
                "form",
                {
                  on: {
                    submit: function($event) {
                      $event.preventDefault()
                    }
                  }
                },
                [
                  _c("vs-input", {
                    directives: [
                      {
                        name: "validate",
                        rawName: "v-validate",
                        value: "required|email",
                        expression: "'required|email'"
                      }
                    ],
                    staticClass: "w-full mb-6",
                    attrs: {
                      name: "mailTo",
                      "label-placeholder": "To",
                      danger: !_vm.validateForm && _vm.mailTo != "",
                      "val-icon-danger": "clear",
                      success: _vm.validateForm,
                      "val-icon-success": "done",
                      color: _vm.validateForm ? "success" : "danger"
                    },
                    model: {
                      value: _vm.mailTo,
                      callback: function($$v) {
                        _vm.mailTo = $$v
                      },
                      expression: "mailTo"
                    }
                  }),
                  _vm._v(" "),
                  _c("vs-input", {
                    staticClass: "w-full mb-6",
                    attrs: {
                      name: "mailSubject",
                      "label-placeholder": "Subject"
                    },
                    model: {
                      value: _vm.mailSubject,
                      callback: function($$v) {
                        _vm.mailSubject = $$v
                      },
                      expression: "mailSubject"
                    }
                  }),
                  _vm._v(" "),
                  _c("vs-input", {
                    staticClass: "w-full mb-6",
                    attrs: { name: "mailCC", "label-placeholder": "CC" },
                    model: {
                      value: _vm.mailCC,
                      callback: function($$v) {
                        _vm.mailCC = $$v
                      },
                      expression: "mailCC"
                    }
                  }),
                  _vm._v(" "),
                  _c("vs-input", {
                    staticClass: "w-full mb-6",
                    attrs: { name: "mailBCC", "label-placeholder": "BCC" },
                    model: {
                      value: _vm.mailBCC,
                      callback: function($$v) {
                        _vm.mailBCC = $$v
                      },
                      expression: "mailBCC"
                    }
                  }),
                  _vm._v(" "),
                  _c("quill-editor", {
                    attrs: { options: _vm.editorOption },
                    model: {
                      value: _vm.mailMessage,
                      callback: function($$v) {
                        _vm.mailMessage = $$v
                      },
                      expression: "mailMessage"
                    }
                  }),
                  _vm._v(" "),
                  _c("vs-upload", {
                    staticClass: "compose-mail-attachment",
                    attrs: {
                      multiple: "",
                      text: "Attachments",
                      "show-upload-button": false
                    }
                  })
                ],
                1
              )
            ]
          )
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "VuePerfectScrollbar",
        {
          key: _vm.$vs.rtl,
          staticClass: "email-filter-scroll-area",
          attrs: { settings: _vm.settings }
        },
        [
          _c(
            "div",
            { staticClass: "px-6 pb-2 flex flex-col" },
            [
              _c(
                "router-link",
                {
                  staticClass:
                    "flex justify-between items-center mt-4 cursor-pointer",
                  class: { "text-primary": _vm.mailFilter == "contact" },
                  attrs: { tag: "span", to: _vm.baseUrl + "/contact" }
                },
                [
                  _c(
                    "div",
                    { staticClass: "flex items-center mb-2" },
                    [
                      _c("feather-icon", {
                        attrs: {
                          icon: "ArchiveIcon",
                          svgClasses: [
                            {
                              "text-primary stroke-current":
                                _vm.mailFilter == "contact"
                            },
                            "h-6 w-6"
                          ]
                        }
                      }),
                      _vm._v(" "),
                      _c("span", { staticClass: "text-lg ml-3" }, [
                        _vm._v("Contact Messages")
                      ])
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _vm.emailMeta.draftMails
                    ? [
                        _vm.contacts.length > 0
                          ? _c(
                              "vs-chip",
                              {
                                staticClass: "number",
                                attrs: { color: "warning" }
                              },
                              [_vm._v(_vm._s(_vm.contacts.length))]
                            )
                          : _vm._e()
                      ]
                    : _vm._e()
                ],
                2
              ),
              _vm._v(" "),
              _c(
                "router-link",
                {
                  staticClass: "flex items-center mt-4 mb-2 cursor-pointer",
                  class: { "text-primary": _vm.mailFilter == "sent" },
                  attrs: { tag: "span", to: _vm.baseUrl + "/sent" }
                },
                [
                  _c(
                    "div",
                    { staticClass: "flex items-center mb-2" },
                    [
                      _c("feather-icon", {
                        attrs: {
                          icon: "SendIcon",
                          svgClasses: [
                            {
                              "text-primary stroke-current":
                                _vm.mailFilter == "sent"
                            },
                            "h-6 w-6"
                          ]
                        }
                      }),
                      _vm._v(" "),
                      _c("span", { staticClass: "text-lg ml-3" }, [
                        _vm._v("Sent Newsletters")
                      ])
                    ],
                    1
                  )
                ]
              ),
              _vm._v(" "),
              _c(
                "router-link",
                {
                  staticClass:
                    "flex justify-between items-center cursor-pointer",
                  class: { "text-primary": _vm.mailFilter == "email" },
                  attrs: { tag: "span", to: _vm.baseUrl + "/email" }
                },
                [
                  _c(
                    "div",
                    { staticClass: "flex items-center mb-2" },
                    [
                      _c("feather-icon", {
                        attrs: {
                          icon: "MailIcon",
                          svgClasses: [
                            {
                              "text-primary stroke-current":
                                _vm.mailFilter == "email"
                            },
                            "h-6 w-6"
                          ]
                        }
                      }),
                      _vm._v(" "),
                      _c("span", { staticClass: "text-lg ml-3" }, [
                        _vm._v("Emails")
                      ])
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _vm.emailMeta.unreadMails
                    ? [
                        _vm.newsLetters.length > 0
                          ? _c(
                              "vs-chip",
                              {
                                staticClass: "number",
                                attrs: { color: "primary" }
                              },
                              [_vm._v(_vm._s(_vm.newsLetters.length))]
                            )
                          : _vm._e()
                      ]
                    : _vm._e()
                ],
                2
              )
            ],
            1
          )
        ]
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/email/NewsLetterItem.vue?vue&type=template&id=04f31974&":
/*!****************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/pages/email/NewsLetterItem.vue?vue&type=template&id=04f31974& ***!
  \****************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "mail__mail-item sm:px-4 px-2 py-6 " }, [
    _c("div", { staticClass: "flex w-full" }, [
      _c("div", { staticClass: "flex w-full justify-between items-start" }, [
        _c("div", { staticClass: "mail__details" }, [
          _c(
            "span",
            {
              staticStyle: {
                float: "right",
                position: "absolute",
                right: "0px",
                "margin-right": "15px"
              }
            },
            [_vm._v(_vm._s(_vm.getEmail(_vm.newsLetter.created_at)))]
          )
        ])
      ])
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "flex w-full" }, [
      _c(
        "div",
        { staticClass: "flex items-center ml-1" },
        [
          _c("vs-checkbox", {
            staticClass: "vs-checkbox-small ml-0 mr-1",
            on: {
              click: function($event) {
                $event.stopPropagation()
              }
            },
            model: {
              value: _vm.isSelectedMail,
              callback: function($$v) {
                _vm.isSelectedMail = $$v
              },
              expression: "isSelectedMail"
            }
          })
        ],
        1
      ),
      _vm._v(" "),
      _c("div", { staticClass: "mail__message truncate ml-3" }, [
        _c("span", [_vm._v(_vm._s(_vm.newsLetter.email))])
      ])
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/src/store/email/moduleEmail.js":
/*!*****************************************************!*\
  !*** ./resources/js/src/store/email/moduleEmail.js ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _moduleEmailState_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./moduleEmailState.js */ "./resources/js/src/store/email/moduleEmailState.js");
/* harmony import */ var _moduleEmailMutations_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./moduleEmailMutations.js */ "./resources/js/src/store/email/moduleEmailMutations.js");
/* harmony import */ var _moduleEmailActions_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./moduleEmailActions.js */ "./resources/js/src/store/email/moduleEmailActions.js");
/* harmony import */ var _moduleEmailGetters_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./moduleEmailGetters.js */ "./resources/js/src/store/email/moduleEmailGetters.js");
/*=========================================================================================
  File Name: moduleEmail.js
  Description: Email Module
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/




/* harmony default export */ __webpack_exports__["default"] = ({
  namespaced: true,
  state: _moduleEmailState_js__WEBPACK_IMPORTED_MODULE_0__["default"],
  mutations: _moduleEmailMutations_js__WEBPACK_IMPORTED_MODULE_1__["default"],
  actions: _moduleEmailActions_js__WEBPACK_IMPORTED_MODULE_2__["default"],
  getters: _moduleEmailGetters_js__WEBPACK_IMPORTED_MODULE_3__["default"]
});

/***/ }),

/***/ "./resources/js/src/store/email/moduleEmailActions.js":
/*!************************************************************!*\
  !*** ./resources/js/src/store/email/moduleEmailActions.js ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _axios_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @/axios.js */ "./resources/js/src/axios.js");
/*=========================================================================================
  File Name: moduleEmailActions.js
  Description: Email Module Actions
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/

/* harmony default export */ __webpack_exports__["default"] = ({
  setEmailSearchQuery: function setEmailSearchQuery(_ref, data) {
    var commit = _ref.commit;

    if (data.filter == 'contact') {
      commit("CONTACT_SEARCH_QUERY", data.searchQuery);
    } else if (data.filter == 'email') {
      commit("EMAIL_SEARCH_QUERY", data.searchQuery);
    }
  },
  // Fetch emails
  fetchEmails: function fetchEmails(context, data) {
    _axios_js__WEBPACK_IMPORTED_MODULE_0__["default"].defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');
    return new Promise(function (resolve, reject) {
      _axios_js__WEBPACK_IMPORTED_MODULE_0__["default"].post("/api/news-letter/index", data).then(function (response) {
        context.commit('SET_NEWS_LETTERS', response.data.newsLetters);
        context.commit('SET_CONTACTS', response.data.contacts);
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  // Move mails to another folder
  deleteRecord: function deleteRecord(context, payload) {
    _axios_js__WEBPACK_IMPORTED_MODULE_0__["default"].defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');
    return new Promise(function (resolve, reject) {
      _axios_js__WEBPACK_IMPORTED_MODULE_0__["default"].post("/api/news-letter/delete", payload).then(function (response) {
        context.commit("REMOVE_RECORD", payload);
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  // Fetch Tags
  fetchTags: function fetchTags(_ref2) {
    var commit = _ref2.commit;
    return new Promise(function (resolve, reject) {
      _axios_js__WEBPACK_IMPORTED_MODULE_0__["default"].get("/api/apps/email/tags").then(function (response) {
        commit("SET_TAGS", response.data);
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  // Fetch Email Meta
  fetchMeta: function fetchMeta(_ref3) {
    var commit = _ref3.commit;
    return new Promise(function (resolve, reject) {
      _axios_js__WEBPACK_IMPORTED_MODULE_0__["default"].get("/api/apps/email/meta").then(function (response) {
        commit("SET_META", response.data);
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  // Update Mails label
  updateLabels: function updateLabels(_ref4, payload) {
    var commit = _ref4.commit;
    return new Promise(function (resolve, reject) {
      _axios_js__WEBPACK_IMPORTED_MODULE_0__["default"].post("/api/apps/email/update-labels", {
        emailIds: payload.mails,
        label: payload.label
      }).then(function (response) {
        commit("UPDATE_LABELS", payload);
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  setLabels: function setLabels(_ref5, payload) {
    var commit = _ref5.commit;
    return new Promise(function (resolve, reject) {
      _axios_js__WEBPACK_IMPORTED_MODULE_0__["default"].post("/api/apps/email/set-labels", {
        mailId: payload.mailId,
        labels: payload.labels
      }).then(function (response) {
        commit("SET_LABELS", payload);
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  // Set mails flag unread to true
  setUnread: function setUnread(_ref6, payload) {
    var commit = _ref6.commit;
    return new Promise(function (resolve, reject) {
      _axios_js__WEBPACK_IMPORTED_MODULE_0__["default"].post("/api/apps/email/mark-unread", {
        emailIds: payload.emailIds,
        unreadFlag: payload.unreadFlag
      }).then(function (response) {
        commit("SET_UNREAD", payload); // Remove this if you are getting meta like us
        // Use your own method to update email meta if you are fetching email meta

        commit("UPDATE_UNREAD_META", payload);
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  // Toggle isStarred flag in mail
  toggleIsMailStarred: function toggleIsMailStarred(_ref7, payload) {
    var commit = _ref7.commit;
    return new Promise(function (resolve, reject) {
      _axios_js__WEBPACK_IMPORTED_MODULE_0__["default"].post("/api/apps/email/set-starred", {
        mailId: payload.mailId,
        value: payload.value
      }).then(function (response) {
        commit("TOGGLE_IS_MAIL_STARRED", payload);
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  }
});

/***/ }),

/***/ "./resources/js/src/store/email/moduleEmailGetters.js":
/*!************************************************************!*\
  !*** ./resources/js/src/store/email/moduleEmailGetters.js ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/*=========================================================================================
  File Name: moduleEmailGetters.js
  Description: Email Module Getters
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/
/* harmony default export */ __webpack_exports__["default"] = ({});

/***/ }),

/***/ "./resources/js/src/store/email/moduleEmailMutations.js":
/*!**************************************************************!*\
  !*** ./resources/js/src/store/email/moduleEmailMutations.js ***!
  \**************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/*=========================================================================================
  File Name: moduleEmailMutations.js
  Description: Email Module Mutations
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/
/* harmony default export */ __webpack_exports__["default"] = ({
  SET_NEWS_LETTERS: function SET_NEWS_LETTERS(state, newsLetters) {
    state.newsLetters = newsLetters;
  },
  SET_CONTACTS: function SET_CONTACTS(state, contacts) {
    state.contacts = contacts;
  },
  REMOVE_RECORD: function REMOVE_RECORD(state, payload) {
    if (payload.filter == 'contact') {
      payload.emailIds.forEach(function (id) {
        var index = state.contacts.findIndex(function (c) {
          return c.id == id;
        });
        state.contacts.splice(index, 1);
      });
    } else if (payload.filter == 'email') {
      payload.emailIds.forEach(function (id) {
        var index = state.newsLetters.findIndex(function (n) {
          return n.id == id;
        });
        state.newsLetters.splice(index, 1);
      });
    }
  },
  SET_EMAIL_SEARCH_QUERY: function SET_EMAIL_SEARCH_QUERY(state, query) {
    state.mailSearchQuery = query;
  },
  SET_MAILS: function SET_MAILS(state, mails) {
    state.mails = mails;
  },
  SET_TAGS: function SET_TAGS(state, tags) {
    state.mailTags = tags;
  },
  SET_META: function SET_META(state, meta) {
    state.meta = meta;
  },
  UPDATE_MAIL_FILTER: function UPDATE_MAIL_FILTER(state, filterName) {
    state.mail_filter = filterName;
  },
  UPDATE_LABELS: function UPDATE_LABELS(state, payload) {
    payload.mails.forEach(function (mailId) {
      var mailIndex = state.mails.findIndex(function (mail) {
        return mail.id == mailId;
      });
      var index = state.mails[mailIndex].labels.indexOf(payload.label);
      if (index == -1) state.mails[mailIndex].labels.push(payload.label);else state.mails[mailIndex].labels.splice(index, 1);
    });
  },
  SET_LABELS: function SET_LABELS(state, payload) {
    state.mails.find(function (mail) {
      return mail.id === payload.mailId;
    }).labels = payload.labels;
  },
  SET_UNREAD: function SET_UNREAD(state, payload) {
    payload.emailIds.forEach(function (mailId) {
      var mailIndex = state.mails.findIndex(function (mail) {
        return mail.id == mailId;
      });
      if (mailIndex !== -1) state.mails[mailIndex].unread = payload.unreadFlag;
    });
  },
  MOVE_MAILS_TO: function MOVE_MAILS_TO(state, payload) {
    payload.emailIds.forEach(function (mailId) {
      var mailIndex = state.mails.findIndex(function (mail) {
        return mail.id == mailId;
      }); // Update draft meta
      // If moving from draft mailFolder decrease draft meta by removing mailId

      state.mails[mailIndex].mailFolder === "draft" ? state.meta.draftMails.splice(state.meta.draftMails.findIndex(function (i) {
        return i === mailId;
      }), 1) : null; // Else increase by pushing mailId

      payload.to === "draft" ? state.meta.draftMails.push(mailId) : null;
      state.mails[mailIndex].mailFolder = payload.to;
    });
  },
  TOGGLE_IS_MAIL_STARRED: function TOGGLE_IS_MAIL_STARRED(state, payload) {
    state.mails.find(function (mail) {
      return mail.id === payload.mailId;
    }).isStarred = payload.value;
  },
  // If your process of fetching is different than ours. Please update action and mutation
  // Maybe this mutation is redundant for you. Feel free to remove it.
  UPDATE_UNREAD_META: function UPDATE_UNREAD_META(state, payload) {
    var _loop = function _loop(folder) {
      // If folder is same as current filter
      if (folder == state.mail_filter) {
        // If unread flag is true - increase count
        if (payload.unreadFlag) {
          payload.emailIds.forEach(function (mailId) {
            if (state.meta.unreadMails.folder[folder].indexOf(mailId) === -1) state.meta.unreadMails.folder[folder].push(mailId);
          });
        } // else reduce unread mails count
        else {
            payload.emailIds.forEach(function (mailId) {
              var mailIdIndex = state.meta.unreadMails.folder[folder].indexOf(mailId);
              if (mailIdIndex !== -1) state.meta.unreadMails.folder[folder].splice(mailIdIndex, 1);
            });
          }
      }
    };

    // Loop over email meta
    for (var folder in state.meta.unreadMails.folder) {
      _loop(folder);
    }
  },
  UPDATE_UNREAD_META_ON_MOVE: function UPDATE_UNREAD_META_ON_MOVE(state, payload) {
    // Updating Draft meta is handled by "MOVE_MAILS_TO" mutation
    payload.emailIds.forEach(function (mailId) {
      var mail = state.mails.find(function (mail) {
        return mail.id == mailId;
      });

      if (mail.unread) {
        var cf_unread_mails = state.meta.unreadMails.folder[state.mail_filter];

        if (cf_unread_mails) {
          cf_unread_mails.splice(cf_unread_mails.indexOf(mailId), 1);
        }

        if (state.meta.unreadMails.folder[payload.to]) state.meta.unreadMails.folder[payload.to].push(mailId);
      }
    });
  }
});

/***/ }),

/***/ "./resources/js/src/store/email/moduleEmailState.js":
/*!**********************************************************!*\
  !*** ./resources/js/src/store/email/moduleEmailState.js ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/*=========================================================================================
  File Name: moduleEmailState.js
  Description: Email Module State
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/
/* harmony default export */ __webpack_exports__["default"] = ({
  mails: [],
  mail_filter: null,
  mailTags: [],
  meta: {},
  mailSearchQuery: '',
  newsLetters: [],
  contacts: []
});

/***/ }),

/***/ "./resources/js/src/views/pages/email/ContactItem.vue":
/*!************************************************************!*\
  !*** ./resources/js/src/views/pages/email/ContactItem.vue ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ContactItem_vue_vue_type_template_id_7226951b___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ContactItem.vue?vue&type=template&id=7226951b& */ "./resources/js/src/views/pages/email/ContactItem.vue?vue&type=template&id=7226951b&");
/* harmony import */ var _ContactItem_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ContactItem.vue?vue&type=script&lang=js& */ "./resources/js/src/views/pages/email/ContactItem.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _ContactItem_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ContactItem_vue_vue_type_template_id_7226951b___WEBPACK_IMPORTED_MODULE_0__["render"],
  _ContactItem_vue_vue_type_template_id_7226951b___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/src/views/pages/email/ContactItem.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/src/views/pages/email/ContactItem.vue?vue&type=script&lang=js&":
/*!*************************************************************************************!*\
  !*** ./resources/js/src/views/pages/email/ContactItem.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ContactItem_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./ContactItem.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/email/ContactItem.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ContactItem_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/src/views/pages/email/ContactItem.vue?vue&type=template&id=7226951b&":
/*!*******************************************************************************************!*\
  !*** ./resources/js/src/views/pages/email/ContactItem.vue?vue&type=template&id=7226951b& ***!
  \*******************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ContactItem_vue_vue_type_template_id_7226951b___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./ContactItem.vue?vue&type=template&id=7226951b& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/email/ContactItem.vue?vue&type=template&id=7226951b&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ContactItem_vue_vue_type_template_id_7226951b___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ContactItem_vue_vue_type_template_id_7226951b___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/src/views/pages/email/Email.vue":
/*!******************************************************!*\
  !*** ./resources/js/src/views/pages/email/Email.vue ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Email_vue_vue_type_template_id_5aae5638___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Email.vue?vue&type=template&id=5aae5638& */ "./resources/js/src/views/pages/email/Email.vue?vue&type=template&id=5aae5638&");
/* harmony import */ var _Email_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Email.vue?vue&type=script&lang=js& */ "./resources/js/src/views/pages/email/Email.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _Email_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Email.vue?vue&type=style&index=0&lang=scss& */ "./resources/js/src/views/pages/email/Email.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _Email_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Email_vue_vue_type_template_id_5aae5638___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Email_vue_vue_type_template_id_5aae5638___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/src/views/pages/email/Email.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/src/views/pages/email/Email.vue?vue&type=script&lang=js&":
/*!*******************************************************************************!*\
  !*** ./resources/js/src/views/pages/email/Email.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Email_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Email.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/email/Email.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Email_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/src/views/pages/email/Email.vue?vue&type=style&index=0&lang=scss&":
/*!****************************************************************************************!*\
  !*** ./resources/js/src/views/pages/email/Email.vue?vue&type=style&index=0&lang=scss& ***!
  \****************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Email_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/style-loader!../../../../../../node_modules/css-loader!../../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../../node_modules/postcss-loader/src??ref--8-2!../../../../../../node_modules/sass-loader/dist/cjs.js??ref--8-3!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Email.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/email/Email.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Email_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Email_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Email_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Email_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Email_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/src/views/pages/email/Email.vue?vue&type=template&id=5aae5638&":
/*!*************************************************************************************!*\
  !*** ./resources/js/src/views/pages/email/Email.vue?vue&type=template&id=5aae5638& ***!
  \*************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Email_vue_vue_type_template_id_5aae5638___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Email.vue?vue&type=template&id=5aae5638& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/email/Email.vue?vue&type=template&id=5aae5638&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Email_vue_vue_type_template_id_5aae5638___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Email_vue_vue_type_template_id_5aae5638___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/src/views/pages/email/EmailSidebar.vue":
/*!*************************************************************!*\
  !*** ./resources/js/src/views/pages/email/EmailSidebar.vue ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _EmailSidebar_vue_vue_type_template_id_e24b6ff0___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./EmailSidebar.vue?vue&type=template&id=e24b6ff0& */ "./resources/js/src/views/pages/email/EmailSidebar.vue?vue&type=template&id=e24b6ff0&");
/* harmony import */ var _EmailSidebar_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./EmailSidebar.vue?vue&type=script&lang=js& */ "./resources/js/src/views/pages/email/EmailSidebar.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _EmailSidebar_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _EmailSidebar_vue_vue_type_template_id_e24b6ff0___WEBPACK_IMPORTED_MODULE_0__["render"],
  _EmailSidebar_vue_vue_type_template_id_e24b6ff0___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/src/views/pages/email/EmailSidebar.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/src/views/pages/email/EmailSidebar.vue?vue&type=script&lang=js&":
/*!**************************************************************************************!*\
  !*** ./resources/js/src/views/pages/email/EmailSidebar.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_EmailSidebar_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./EmailSidebar.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/email/EmailSidebar.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_EmailSidebar_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/src/views/pages/email/EmailSidebar.vue?vue&type=template&id=e24b6ff0&":
/*!********************************************************************************************!*\
  !*** ./resources/js/src/views/pages/email/EmailSidebar.vue?vue&type=template&id=e24b6ff0& ***!
  \********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_EmailSidebar_vue_vue_type_template_id_e24b6ff0___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./EmailSidebar.vue?vue&type=template&id=e24b6ff0& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/email/EmailSidebar.vue?vue&type=template&id=e24b6ff0&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_EmailSidebar_vue_vue_type_template_id_e24b6ff0___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_EmailSidebar_vue_vue_type_template_id_e24b6ff0___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/src/views/pages/email/NewsLetterItem.vue":
/*!***************************************************************!*\
  !*** ./resources/js/src/views/pages/email/NewsLetterItem.vue ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _NewsLetterItem_vue_vue_type_template_id_04f31974___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./NewsLetterItem.vue?vue&type=template&id=04f31974& */ "./resources/js/src/views/pages/email/NewsLetterItem.vue?vue&type=template&id=04f31974&");
/* harmony import */ var _NewsLetterItem_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./NewsLetterItem.vue?vue&type=script&lang=js& */ "./resources/js/src/views/pages/email/NewsLetterItem.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _NewsLetterItem_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _NewsLetterItem_vue_vue_type_template_id_04f31974___WEBPACK_IMPORTED_MODULE_0__["render"],
  _NewsLetterItem_vue_vue_type_template_id_04f31974___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/src/views/pages/email/NewsLetterItem.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/src/views/pages/email/NewsLetterItem.vue?vue&type=script&lang=js&":
/*!****************************************************************************************!*\
  !*** ./resources/js/src/views/pages/email/NewsLetterItem.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_NewsLetterItem_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./NewsLetterItem.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/email/NewsLetterItem.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_NewsLetterItem_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/src/views/pages/email/NewsLetterItem.vue?vue&type=template&id=04f31974&":
/*!**********************************************************************************************!*\
  !*** ./resources/js/src/views/pages/email/NewsLetterItem.vue?vue&type=template&id=04f31974& ***!
  \**********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_NewsLetterItem_vue_vue_type_template_id_04f31974___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./NewsLetterItem.vue?vue&type=template&id=04f31974& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/email/NewsLetterItem.vue?vue&type=template&id=04f31974&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_NewsLetterItem_vue_vue_type_template_id_04f31974___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_NewsLetterItem_vue_vue_type_template_id_04f31974___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);