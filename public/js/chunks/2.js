(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[2],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/components/product-master/MultiplePictures.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/components/product-master/MultiplePictures.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  props: {
    prodAttribute: {
      required: true
    },
    attribute: {
      required: true
    }
  },
  data: function data() {
    return {
      productAttribute: this.prodAttribute
    };
  },
  created: function created() {},
  computed: {
    value: function value() {
      return this.productAttribute.value;
    }
  },
  methods: {
    updateCurrImg: function updateCurrImg(input) {
      var vm = this;
      var selectedFiles = input.target.files;

      for (var i = 0; i < selectedFiles.length; i++) {
        this.productAttribute.value.push(selectedFiles[i]);
      }

      this.picturesPreview();
    },
    removePicture: function removePicture(index) {
      this.productAttribute.value.splice(index, 1);
      this.picturesPreview();
    },
    picturesPreview: function picturesPreview() {
      var _this = this;

      var _loop = function _loop(i) {
        var reader = new FileReader();

        reader.onload = function (e) {
          _this.$refs.image[i].src = reader.result;
        };

        reader.readAsDataURL(_this.productAttribute.value[i]);
      };

      for (var i = 0; i < this.productAttribute.value.length; i++) {
        _loop(i);
      }
    }
  },
  components: {}
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/components/product-master/ProductAttributesValues.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/components/product-master/ProductAttributesValues.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _SinglePicture_vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./SinglePicture.vue */ "./resources/js/src/components/product-master/SinglePicture.vue");
/* harmony import */ var _MultiplePictures_vue__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./MultiplePictures.vue */ "./resources/js/src/components/product-master/MultiplePictures.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  props: {
    itemAttributes: {
      type: Array,
      required: true
    },
    isSidebarActiveLocal: {
      required: true
    },
    prodAttributes: {
      required: true
    },
    values: {
      required: true
    }
  },
  data: function data() {
    return {
      productAttributes: this.prodAttributes,
      dataImg: null,
      images: []
    };
  },
  created: function created() {
    var _this = this;

    this.itemAttributes.forEach(function (itemAttribute, index) {
      if (itemAttribute.data_type === "String") {
        _this.productAttributes[index] = {
          attribute_id: itemAttribute.id,
          data_type: itemAttribute.data_type,
          value: ""
        };
      } else if (itemAttribute.data_type === "Number") {
        _this.productAttributes[index] = {
          attribute_id: itemAttribute.id,
          data_type: itemAttribute.data_type,
          value: 0
        };
      } else if (itemAttribute.data_type === "Picture") {
        _this.productAttributes[index] = {
          attribute_id: itemAttribute.id,
          data_type: itemAttribute.data_type,
          value: ""
        };
      } else if (itemAttribute.data_type === "Multiple Pictures") {
        _this.productAttributes[index] = {
          attribute_id: itemAttribute.id,
          data_type: itemAttribute.data_type,
          value: []
        };
      } else {
        _this.productAttributes[index] = {
          attribute_id: itemAttribute.id,
          data_type: itemAttribute.data_type,
          value: ""
        };
      }
    });
  },
  computed: {},
  methods: {},
  components: {
    SinglePicture: _SinglePicture_vue__WEBPACK_IMPORTED_MODULE_0__["default"],
    MultiplePictures: _MultiplePictures_vue__WEBPACK_IMPORTED_MODULE_1__["default"]
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/components/product-master/ProductCatalogSelection.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/components/product-master/ProductCatalogSelection.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  props: {
    itemCatalogs: {
      type: Array,
      required: true
    },
    nextStep: {
      required: true
    },
    isSidebarActiveLocal: {
      required: true
    },
    values: {
      required: true
    }
  },
  data: function data() {
    return {};
  },
  methods: {
    continueToNextStep: function continueToNextStep(catalog) {
      var _this = this;

      this.values.catalog_id = catalog.id;
      this.values.catalog_name = catalog.name;
      this.$store.dispatch("productMaster/fetchItemAttributes", this.values.item_id).then(function (response) {
        return _this.nextStep();
      }).catch(function (err) {
        console.error(err);
      });
    }
  },
  components: {}
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/components/product-master/ProductDetails.vue?vue&type=script&lang=js&":
/*!********************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/components/product-master/ProductDetails.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _SinglePicture_vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./SinglePicture.vue */ "./resources/js/src/components/product-master/SinglePicture.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  props: {
    isSidebarActiveLocal: {
      required: true
    },
    values: {
      required: true
    }
  },
  data: function data() {
    return {
      title: this.values.title,
      imageSrc: {
        value: this.values.image_url
      },
      mirrorImageSrc: {
        value: this.values.mirror_image_url
      },
      on_sale: this.values.on_sale,
      new_product: this.values.new_product,
      offer: this.values.offer,
      discount: this.values.discount,
      old_price: this.values.old_price,
      new_price: this.values.new_price,
      discount_off: this.values.discount_off,
      description: this.values.description
    };
  },
  created: function created() {},
  watch: {
    imageSrc: function imageSrc(val) {
      this.values.image_src = this.imageSrc.value;
    },
    mirrorImageSrc: function mirrorImageSrc(val) {
      this.values.mirror_image_src = this.mirrorImageSrc.value;
    }
  },
  computed: {},
  methods: {
    onChange: function onChange() {
      this.values.image_src = this.imageSrc.value;
      this.values.mirror_image_src = this.mirrorImageSrc.value;
    }
  },
  components: {
    SinglePicture: _SinglePicture_vue__WEBPACK_IMPORTED_MODULE_0__["default"]
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/components/product-master/ProductItemSelection.vue?vue&type=script&lang=js&":
/*!**************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/components/product-master/ProductItemSelection.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  props: {
    productItems: {
      type: Array,
      required: true
    },
    nextStep: {
      required: true
    },
    isSidebarActiveLocal: {
      required: true
    },
    values: {
      required: true
    }
  },
  data: function data() {
    return {};
  },
  methods: {
    continueToNextStep: function continueToNextStep(item) {
      var _this = this;

      this.values.item_id = item.id;
      this.values.item_name = item.name;
      console.log(item);
      this.$store.dispatch("productMaster/fetchItemCatalogs", item.id).then(function (response) {
        return _this.nextStep();
      }).catch(function (err) {
        console.error(err);
      });
    }
  },
  components: {}
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/components/product-master/SinglePicture.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/components/product-master/SinglePicture.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  props: {
    prodAttribute: {
      required: true
    },
    attribute: {
      required: true
    },
    onChange: {
      required: false
    }
  },
  data: function data() {
    return {
      productAttribute: this.prodAttribute
    };
  },
  created: function created() {},
  computed: {
    value: function value() {
      return this.productAttribute.value;
    }
  },
  methods: {
    updateCurrImg: function updateCurrImg(input) {
      var _this = this;

      if (input.target.files && input.target.files[0]) {
        this.productAttribute.value = input.target.files[0];
        var reader = new FileReader();

        reader.onload = function (e) {
          _this.productAttribute = _objectSpread(_objectSpread({}, _this.productAttribute), {}, {
            value: e.target.result
          });
        };

        reader.readAsDataURL(input.target.files[0]);
        this.onChange ? this.onChange() : "";
      }
    }
  },
  components: {}
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/product-master/ProductMaster.vue?vue&type=script&lang=js&":
/*!********************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/pages/product-master/ProductMaster.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ProductMasterSidebar_vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ProductMasterSidebar.vue */ "./resources/js/src/views/pages/product-master/ProductMasterSidebar.vue");
/* harmony import */ var _store_productMaster_moduleProductMaster_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @/store/productMaster/moduleProductMaster.js */ "./resources/js/src/store/productMaster/moduleProductMaster.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    ProductMasterSidebar: _ProductMasterSidebar_vue__WEBPACK_IMPORTED_MODULE_0__["default"]
  },
  data: function data() {
    return {
      selected: [],
      //   products: [],
      itemsPerPage: 4,
      isMounted: false,
      // Data Sidebar
      addNewDataSidebar: false,
      sidebarData: {}
    };
  },
  computed: {
    currentPage: function currentPage() {
      if (this.isMounted) {
        return this.$refs.table.currentx;
      }

      return 0;
    },
    products: function products() {
      // console.log(this.$store.state.productMaster.products);
      return this.$store.state.productMaster.products;
    },
    queriedItems: function queriedItems() {
      return this.$refs.table ? this.$refs.table.queriedResults.length : this.products.length;
    }
  },
  methods: {
    addNewData: function addNewData() {
      this.sidebarData = {};
      this.toggleDataSidebar(true);
    },
    deleteData: function deleteData(id) {
      this.$store.dispatch("productMaster/removeProduct", id).catch(function (err) {
        console.error(err);
      });
    },
    editData: function editData(data) {
      // this.sidebarData = JSON.parse(JSON.stringify(this.blankData))
      this.sidebarData = data;
      this.toggleDataSidebar(true);
    },
    getOrderStatusColor: function getOrderStatusColor(status) {
      if (status == 'on_hold') return "warning";
      if (status == 'delivered') return "success";
      if (status == 'canceled') return "danger";
      return "primary";
    },
    getPopularityColor: function getPopularityColor(num) {
      if (num > 90) return "success";
      if (num > 70) return "primary";
      if (num >= 50) return "warning";
      if (num < 50) return "danger";
      return "primary";
    },
    toggleDataSidebar: function toggleDataSidebar() {
      var val = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;
      this.addNewDataSidebar = val;
    },
    toggleStatus: function toggleStatus() {
      var _this = this;

      var selectedData = [];
      this.selected.forEach(function (element) {
        var e = {
          id: element.id,
          status: element.status
        };
        selectedData.push(e);
      });
      this.$store.dispatch('productMaster/toggleStatus', selectedData).then(function (response) {
        _this.selected = [];
      });
    }
  },
  created: function created() {
    if (!_store_productMaster_moduleProductMaster_js__WEBPACK_IMPORTED_MODULE_1__["default"].isRegistered) {
      this.$store.registerModule('productMaster', _store_productMaster_moduleProductMaster_js__WEBPACK_IMPORTED_MODULE_1__["default"]);
      _store_productMaster_moduleProductMaster_js__WEBPACK_IMPORTED_MODULE_1__["default"].isRegistered = true;
    }

    this.$store.dispatch("productMaster/fetchProducts");
  },
  mounted: function mounted() {
    this.isMounted = true;
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/product-master/ProductMasterSidebar.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/pages/product-master/ProductMasterSidebar.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue-perfect-scrollbar */ "./node_modules/vue-perfect-scrollbar/dist/index.js");
/* harmony import */ var vue_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(vue_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _store_productMaster_moduleProductMaster_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @/store/productMaster/moduleProductMaster.js */ "./resources/js/src/store/productMaster/moduleProductMaster.js");
/* harmony import */ var _components_product_master_ProductItemSelection__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @/components/product-master/ProductItemSelection */ "./resources/js/src/components/product-master/ProductItemSelection.vue");
/* harmony import */ var _components_product_master_ProductCatalogSelection__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @/components/product-master/ProductCatalogSelection */ "./resources/js/src/components/product-master/ProductCatalogSelection.vue");
/* harmony import */ var _components_product_master_ProductDetails__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @/components/product-master/ProductDetails */ "./resources/js/src/components/product-master/ProductDetails.vue");
/* harmony import */ var _components_product_master_ProductAttributesValues__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @/components/product-master/ProductAttributesValues */ "./resources/js/src/components/product-master/ProductAttributesValues.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//






/* harmony default export */ __webpack_exports__["default"] = ({
  props: {
    isSidebarActive: {
      type: Boolean,
      required: true
    },
    data: {
      type: Object,
      default: function _default() {}
    }
  },
  watch: {
    isSidebarActive: function isSidebarActive(val) {
      if (!val) return;

      if (Object.entries(this.data).length === 0) {
        this.initValues();
        this.$validator.reset();
      } else {
        //   console.log(JSON.parse(JSON.stringify(this.data)))
        var _JSON$parse = JSON.parse(JSON.stringify(this.data)),
            id = _JSON$parse.id,
            title = _JSON$parse.title,
            image_src = _JSON$parse.image_src,
            mirror_image_src = _JSON$parse.mirror_image_src,
            on_sale = _JSON$parse.on_sale,
            new_product = _JSON$parse.new_product,
            offer = _JSON$parse.offer,
            discount = _JSON$parse.discount,
            old_price = _JSON$parse.old_price,
            new_price = _JSON$parse.new_price,
            discount_off = _JSON$parse.discount_off,
            description = _JSON$parse.description,
            catalog_id = _JSON$parse.catalog_id,
            catalog_name = _JSON$parse.catalog_name,
            item_id = _JSON$parse.item_id,
            item_name = _JSON$parse.item_name,
            productAttributes = _JSON$parse.productAttributes;

        this.values.id = id;
        this.values.title = title;
        this.values.image_src = image_src;
        this.values.mirror_image_src = mirror_image_src;
        this.values.on_sale = on_sale;
        this.values.new_product = new_product;
        this.values.offer = offer;
        this.values.discount = discount;
        this.values.old_price = old_price;
        this.values.new_price = new_price;
        this.values.discount_off = discount_off;
        this.values.description = description;
        this.values.catalog_id = catalog_id;
        this.values.catalog_name = catalog_name;
        this.values.item_id = item_id;
        this.values.item_name = item_name;
        this.values.productAttributes = productAttribute;
        this.initValues();
      } // Object.entries(this.data).length === 0 ? this.initValues() : { this.dw_item_id, this.dataName, this.dataCategory, this.dataOrder_status, this.dataPrice } = JSON.parse(JSON.stringify(this.data))

    }
  },
  data: function data() {
    return {
      step: 1,
      values: {
        title: "",
        image_src: null,
        mirror_image_src: null,
        on_sale: "true",
        new_product: "true",
        offer: "false",
        discount: "false",
        old_price: 0,
        new_price: 0,
        discount_off: 0,
        description: "",
        item_id: "",
        item_name: "",
        catalog_id: "",
        catalog_name: "",
        productAttributes: []
      },
      settings: {
        // perfectscrollbar settings
        maxScrollbarLength: 60,
        wheelSpeed: 0.6
      }
    };
  },
  created: function created() {
    if (!_store_productMaster_moduleProductMaster_js__WEBPACK_IMPORTED_MODULE_1__["default"].isRegistered) {
      this.$store.registerModule("productMaster", _store_productMaster_moduleProductMaster_js__WEBPACK_IMPORTED_MODULE_1__["default"]);
      _store_productMaster_moduleProductMaster_js__WEBPACK_IMPORTED_MODULE_1__["default"].isRegistered = true;
    }

    this.$store.dispatch("productMaster/fetchItems");
  },
  computed: {
    isSidebarActiveLocal: {
      get: function get() {
        return this.isSidebarActive;
      },
      set: function set(val) {
        if (!val) {
          this.$emit("closeSidebar"); // this.$validator.reset()
          // this.initValues()
        }
      }
    },
    isFormValid: function isFormValid() {
      return !this.errors.any() && this.values.title && this.values.image_src && this.values.mirror_image_src && this.values.on_sale && this.values.new_product && this.values.offer && this.values.discount && this.values.old_price && this.values.description && this.values.catalog_id && this.values.catalog_name && this.values.item_id && this.values.item_name;
    },
    productItems: function productItems() {
      return this.$store.state.productMaster.productItems;
    },
    itemCatalogs: function itemCatalogs() {
      return this.$store.state.productMaster.itemCatalogs;
    },
    itemAttributes: function itemAttributes() {
      return this.$store.state.productMaster.itemAttributes;
    }
  },
  methods: {
    // Proceed to next step
    nextStep: function nextStep() {
      this.step++;
    },
    // Go back to prev step
    prevStep: function prevStep() {
      this.step--;
    },
    initValues: function initValues() {
      if (this.data.id) return;
      this.values.id = null;
      this.values.title = "";
      this.values.image_src = null;
      this.values.mirror_image_src = null;
      this.values.on_sale = "true";
      this.values.new_product = "true";
      this.values.offer = "false";
      this.values.discount = "false";
      this.values.old_price = 0;
      this.values.new_price = 0;
      this.values.discount_off = 0;
      this.values.description = "";
      this.values.catalog_id = null;
      this.values.catalog_name = "";
      this.values.item_id = null;
      this.values.item_name = "";
      this.values.productAttributes = [];
    },
    submitData: function submitData() {
      var _this = this;

      this.$validator.validateAll().then(function (result) {
        if (result) {
          _this.step = 1;
          var data = new FormData();

          _this.values.productAttributes.forEach(function (productAttribute) {
            if (productAttribute.data_type === "Picture") {
              data.append("singlePictures[".concat(productAttribute.attribute_id, "]"), productAttribute.value);
            }

            if (productAttribute.data_type === "Multiple Pictures") {
              data.append("multiplePicturesLength".concat(productAttribute.attribute_id), productAttribute.value.length);
              productAttribute.value.forEach(function (value, index) {
                var dynamicFormula = (index + 1) * (productAttribute.attribute_id + 1);
                data.append("multiplePictures[".concat(dynamicFormula, "]"), value);
              });
            }
          });

          data.append("imageUrl", _this.values.image_src);
          data.append("mirrorImageUrl", _this.values.mirror_image_src);
          var obj = {
            id: _this.values.id,
            title: _this.values.title,
            on_sale: _this.values.on_sale,
            new_product: _this.values.new_product,
            offer: _this.values.offer,
            discount: _this.values.discount,
            old_price: _this.values.old_price,
            new_price: _this.values.new_price,
            discount_off: _this.values.discount_off,
            description: _this.values.description,
            catalog_id: _this.values.catalog_id,
            catalog_name: _this.values.catalog_name,
            item_id: _this.values.item_id,
            item_name: _this.values.item_name,
            productAttributes: _this.values.productAttributes
          }; // console.log(obj);

          if (_this.values.id !== null && _this.values.id >= 0) {
            data.append("productAttributes", _this.values.productAttributes);
            var json = JSON.stringify(obj);
            data.append("data", json); // console.log(obj)

            _this.$store.dispatch("productMaster/updateProduct", data).catch(function (err) {
              console.error(err);
            });
          } else {
            delete obj.id;
            data.append("productAttributes", _this.values.productAttributes);

            var _json = JSON.stringify(obj);

            data.append("data", _json);

            _this.$store.dispatch("productMaster/addProduct", data).catch(function (err) {
              console.error(err);
            });
          }

          _this.$emit("closeSidebar");

          _this.initValues();
        }
      });
    }
  },
  components: {
    VuePerfectScrollbar: vue_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_0___default.a,
    ProductItemSelection: _components_product_master_ProductItemSelection__WEBPACK_IMPORTED_MODULE_2__["default"],
    ProductCatalogSelection: _components_product_master_ProductCatalogSelection__WEBPACK_IMPORTED_MODULE_3__["default"],
    ProductAttributesValues: _components_product_master_ProductAttributesValues__WEBPACK_IMPORTED_MODULE_5__["default"],
    ProductDetails: _components_product_master_ProductDetails__WEBPACK_IMPORTED_MODULE_4__["default"]
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/product-master/ProductMaster.vue?vue&type=style&index=0&lang=scss&":
/*!*******************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--8-2!./node_modules/sass-loader/dist/cjs.js??ref--8-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/pages/product-master/ProductMaster.vue?vue&type=style&index=0&lang=scss& ***!
  \*******************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "#data-list-list-view .vs-con-table {\n  /*\n    Below media-queries is fix for responsiveness of action buttons\n    Note: If you change action buttons or layout of this page, Please remove below style\n  */\n}\n@media (max-width: 689px) {\n#data-list-list-view .vs-con-table .vs-table--search {\n    max-width: unset;\n    width: 100%;\n}\n[dir=ltr] #data-list-list-view .vs-con-table .vs-table--search {\n    margin-left: 0;\n}\n[dir=rtl] #data-list-list-view .vs-con-table .vs-table--search {\n    margin-right: 0;\n}\n#data-list-list-view .vs-con-table .vs-table--search .vs-table--search-input {\n    width: 100%;\n}\n}\n@media (max-width: 461px) {\n#data-list-list-view .vs-con-table .items-per-page-handler {\n    display: none;\n}\n}\n@media (max-width: 341px) {\n#data-list-list-view .vs-con-table .data-list-btn-container {\n    width: 100%;\n}\n#data-list-list-view .vs-con-table .data-list-btn-container .dd-actions,\n#data-list-list-view .vs-con-table .data-list-btn-container .btn-add-new {\n    width: 100%;\n}\n[dir=ltr] #data-list-list-view .vs-con-table .data-list-btn-container .dd-actions, [dir=ltr] #data-list-list-view .vs-con-table .data-list-btn-container .btn-add-new {\n    margin-right: 0 !important;\n}\n[dir=rtl] #data-list-list-view .vs-con-table .data-list-btn-container .dd-actions, [dir=rtl] #data-list-list-view .vs-con-table .data-list-btn-container .btn-add-new {\n    margin-left: 0 !important;\n}\n}\n#data-list-list-view .vs-con-table .product-name {\n  max-width: 23rem;\n}\n#data-list-list-view .vs-con-table .vs-table--header {\n  display: flex;\n  flex-wrap: wrap;\n}\n[dir=ltr] #data-list-list-view .vs-con-table .vs-table--header {\n  margin-left: 1.5rem;\n  margin-right: 1.5rem;\n}\n[dir=rtl] #data-list-list-view .vs-con-table .vs-table--header {\n  margin-right: 1.5rem;\n  margin-left: 1.5rem;\n}\n#data-list-list-view .vs-con-table .vs-table--header > span {\n  display: flex;\n  flex-grow: 1;\n}\n[dir] #data-list-list-view .vs-con-table .vs-table--header .vs-table--search {\n  padding-top: 0;\n}\n#data-list-list-view .vs-con-table .vs-table--header .vs-table--search .vs-table--search-input {\n  font-size: 1rem;\n}\n[dir] #data-list-list-view .vs-con-table .vs-table--header .vs-table--search .vs-table--search-input {\n  padding: 0.9rem 2.5rem;\n}\n[dir=ltr] #data-list-list-view .vs-con-table .vs-table--header .vs-table--search .vs-table--search-input + i {\n  left: 1rem;\n}\n[dir=rtl] #data-list-list-view .vs-con-table .vs-table--header .vs-table--search .vs-table--search-input + i {\n  right: 1rem;\n}\n[dir=ltr] #data-list-list-view .vs-con-table .vs-table--header .vs-table--search .vs-table--search-input:focus + i {\n  left: 1rem;\n}\n[dir=rtl] #data-list-list-view .vs-con-table .vs-table--header .vs-table--search .vs-table--search-input:focus + i {\n  right: 1rem;\n}\n#data-list-list-view .vs-con-table .vs-table {\n  border-collapse: separate;\n  border-spacing: 0 1.3rem;\n}\n[dir] #data-list-list-view .vs-con-table .vs-table {\n  padding: 0 1rem;\n}\n[dir] #data-list-list-view .vs-con-table .vs-table tr {\n  box-shadow: 0 4px 20px 0 rgba(0, 0, 0, 0.05);\n}\n[dir] #data-list-list-view .vs-con-table .vs-table tr td {\n  padding: 20px;\n}\n[dir=ltr] #data-list-list-view .vs-con-table .vs-table tr td:first-child {\n  border-top-left-radius: 0.5rem;\n  border-bottom-left-radius: 0.5rem;\n}\n[dir=rtl] #data-list-list-view .vs-con-table .vs-table tr td:first-child {\n  border-top-right-radius: 0.5rem;\n  border-bottom-right-radius: 0.5rem;\n}\n[dir=ltr] #data-list-list-view .vs-con-table .vs-table tr td:last-child {\n  border-top-right-radius: 0.5rem;\n  border-bottom-right-radius: 0.5rem;\n}\n[dir=rtl] #data-list-list-view .vs-con-table .vs-table tr td:last-child {\n  border-top-left-radius: 0.5rem;\n  border-bottom-left-radius: 0.5rem;\n}\n[dir] #data-list-list-view .vs-con-table .vs-table tr td.td-check {\n  padding: 20px !important;\n}\n[dir] #data-list-list-view .vs-con-table .vs-table--thead th {\n  padding-top: 0;\n  padding-bottom: 0;\n}\n#data-list-list-view .vs-con-table .vs-table--thead th .vs-table-text {\n  text-transform: uppercase;\n  font-weight: 600;\n}\n[dir] #data-list-list-view .vs-con-table .vs-table--thead th.td-check {\n  padding: 0 15px !important;\n}\n[dir] #data-list-list-view .vs-con-table .vs-table--thead tr {\n  background: none;\n  box-shadow: none;\n}\n#data-list-list-view .vs-con-table .vs-table--pagination {\n  justify-content: center;\n}\n#data-list-list-view .inactive {\n  opacity: 0.4 !important;\n  color: gray !important;\n}", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/product-master/ProductMasterSidebar.vue?vue&type=style&index=0&id=fa7dd856&lang=scss&scoped=true&":
/*!**************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--8-2!./node_modules/sass-loader/dist/cjs.js??ref--8-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/pages/product-master/ProductMasterSidebar.vue?vue&type=style&index=0&id=fa7dd856&lang=scss&scoped=true& ***!
  \**************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".add-new-data-sidebar[data-v-fa7dd856]  .vs-sidebar--background {\n  z-index: 52010;\n}\n.add-new-data-sidebar[data-v-fa7dd856]  .vs-sidebar {\n  z-index: 52010;\n  width: 400px;\n  max-width: 90vw;\n}\n[dir] .add-new-data-sidebar[data-v-fa7dd856]  .vs-sidebar .img-upload {\n  margin-top: 2rem;\n}\n[dir] .add-new-data-sidebar[data-v-fa7dd856]  .vs-sidebar .img-upload .con-img-upload {\n  padding: 0;\n}\n.add-new-data-sidebar[data-v-fa7dd856]  .vs-sidebar .img-upload .con-input-upload {\n  width: 100%;\n}\n[dir] .add-new-data-sidebar[data-v-fa7dd856]  .vs-sidebar .img-upload .con-input-upload {\n  margin: 0;\n}\n.scroll-area--data-list-add-new[data-v-fa7dd856] {\n  height: calc(var(--vh, 1vh) * 100 - 16px - 45px - 82px);\n}", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/product-master/ProductMaster.vue?vue&type=style&index=0&lang=scss&":
/*!***********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--8-2!./node_modules/sass-loader/dist/cjs.js??ref--8-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/pages/product-master/ProductMaster.vue?vue&type=style&index=0&lang=scss& ***!
  \***********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../../../node_modules/css-loader!../../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../../node_modules/postcss-loader/src??ref--8-2!../../../../../../node_modules/sass-loader/dist/cjs.js??ref--8-3!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./ProductMaster.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/product-master/ProductMaster.vue?vue&type=style&index=0&lang=scss&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/product-master/ProductMasterSidebar.vue?vue&type=style&index=0&id=fa7dd856&lang=scss&scoped=true&":
/*!******************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--8-2!./node_modules/sass-loader/dist/cjs.js??ref--8-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/pages/product-master/ProductMasterSidebar.vue?vue&type=style&index=0&id=fa7dd856&lang=scss&scoped=true& ***!
  \******************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../../../node_modules/css-loader!../../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../../node_modules/postcss-loader/src??ref--8-2!../../../../../../node_modules/sass-loader/dist/cjs.js??ref--8-3!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./ProductMasterSidebar.vue?vue&type=style&index=0&id=fa7dd856&lang=scss&scoped=true& */ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/product-master/ProductMasterSidebar.vue?vue&type=style&index=0&id=fa7dd856&lang=scss&scoped=true&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/components/product-master/MultiplePictures.vue?vue&type=template&id=494237c4&":
/*!**************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/components/product-master/MultiplePictures.vue?vue&type=template&id=494237c4& ***!
  \**************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "fragment",
    [
      _vm._l(_vm.productAttribute.value, function(image, key) {
        return _c("div", { key: key }, [
          _c(
            "div",
            {
              staticClass:
                "img-container w-64 mx-auto flex items-center justify-center"
            },
            [
              _c("img", {
                ref: "image",
                refInFor: true,
                staticClass: "responsive",
                attrs: { src: image, alt: "img" }
              })
            ]
          ),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "modify-img flex justify-between mt-5" },
            [
              _c(
                "vs-button",
                {
                  attrs: { type: "flat", color: "#999" },
                  on: {
                    click: function($event) {
                      return _vm.removePicture(key)
                    }
                  }
                },
                [_vm._v("Remove")]
              )
            ],
            1
          )
        ])
      }),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "upload-img mt-5" },
        [
          _c("input", {
            ref: "uploadImgInput",
            staticClass: "hidden",
            attrs: { type: "file", accept: "image/*", multiple: "" },
            on: { change: _vm.updateCurrImg }
          }),
          _vm._v(" "),
          _c(
            "vs-button",
            {
              on: {
                click: function($event) {
                  return _vm.$refs.uploadImgInput.click()
                }
              }
            },
            [_vm._v("Upload " + _vm._s(_vm.attribute.name))]
          )
        ],
        1
      )
    ],
    2
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/components/product-master/ProductAttributesValues.vue?vue&type=template&id=b52c524e&":
/*!*********************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/components/product-master/ProductAttributesValues.vue?vue&type=template&id=b52c524e& ***!
  \*********************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("fragment", [
    _c("div", { staticClass: "mt-6 flex items-center justify-between px-6" }, [
      _c("h4", [_vm._v("ENTER ATTRIBUTES VALUES")])
    ]),
    _vm._v(" "),
    _c(
      "div",
      { staticClass: "p-6" },
      [
        _vm.itemAttributes.length === 0
          ? _c("h3", [
              _vm._v("\n      No Attributes are attached to this item\n    ")
            ])
          : _vm._e(),
        _vm._v(" "),
        _vm._l(_vm.itemAttributes, function(itemAttribute, index) {
          return _c(
            "div",
            { key: index, staticClass: "demo-alignment" },
            [
              itemAttribute.data_type === "Picture"
                ? _c("single-picture", {
                    attrs: {
                      attribute: itemAttribute,
                      prodAttribute: _vm.productAttributes[index]
                    }
                  })
                : _vm._e(),
              _vm._v(" "),
              itemAttribute.data_type === "Multiple Pictures"
                ? _c("multiple-pictures", {
                    attrs: {
                      attribute: itemAttribute,
                      prodAttribute: _vm.productAttributes[index]
                    }
                  })
                : _vm._e(),
              _vm._v(" "),
              itemAttribute.data_type === "String"
                ? _c("vs-input", {
                    directives: [
                      {
                        name: "validate",
                        rawName: "v-validate",
                        value: "required",
                        expression: "'required'"
                      }
                    ],
                    staticClass: "mt-5 w-full",
                    attrs: {
                      label: itemAttribute.name,
                      placeholder: "Attribute Unit is " + itemAttribute.unit,
                      name: itemAttribute.name
                    },
                    model: {
                      value: _vm.productAttributes[index].value,
                      callback: function($$v) {
                        _vm.$set(_vm.productAttributes[index], "value", $$v)
                      },
                      expression: "productAttributes[index].value"
                    }
                  })
                : _vm._e(),
              _vm._v(" "),
              _c(
                "span",
                {
                  directives: [
                    {
                      name: "show",
                      rawName: "v-show",
                      value: _vm.errors.has(itemAttribute.name),
                      expression: "errors.has(itemAttribute.name)"
                    }
                  ],
                  staticClass: "text-danger text-sm"
                },
                [_vm._v(_vm._s(_vm.errors.first(itemAttribute.name)))]
              ),
              _vm._v(" "),
              itemAttribute.data_type === "Number"
                ? _c("label", { staticClass: "vs-input--label" }, [
                    _vm._v(_vm._s(itemAttribute.name))
                  ])
                : _vm._e(),
              _vm._v(" "),
              itemAttribute.data_type === "Number"
                ? _c("vs-input-number", {
                    directives: [
                      {
                        name: "validate",
                        rawName: "v-validate",
                        value: "required",
                        expression: "'required'"
                      }
                    ],
                    staticClass: "mt-5 w-full",
                    attrs: {
                      name: itemAttribute.name,
                      placeholder: "Attribute Unit is " + itemAttribute.unit
                    },
                    model: {
                      value: _vm.productAttributes[index].value,
                      callback: function($$v) {
                        _vm.$set(_vm.productAttributes[index], "value", $$v)
                      },
                      expression: "productAttributes[index].value"
                    }
                  })
                : _vm._e(),
              _vm._v(" "),
              _c(
                "span",
                {
                  directives: [
                    {
                      name: "show",
                      rawName: "v-show",
                      value: _vm.errors.has(itemAttribute.name),
                      expression: "errors.has(itemAttribute.name)"
                    }
                  ],
                  staticClass: "text-danger text-sm"
                },
                [_vm._v(_vm._s(_vm.errors.first(itemAttribute.name)))]
              ),
              _vm._v(" "),
              itemAttribute.data_type === "Textarea"
                ? _c("vs-textarea", {
                    directives: [
                      {
                        name: "validate",
                        rawName: "v-validate",
                        value: "required",
                        expression: "'required'"
                      }
                    ],
                    staticClass: "mt-3",
                    attrs: {
                      label: itemAttribute.name,
                      placeholder: "Attribute Unit is " + itemAttribute.unit
                    },
                    model: {
                      value: _vm.productAttributes[index].value,
                      callback: function($$v) {
                        _vm.$set(_vm.productAttributes[index], "value", $$v)
                      },
                      expression: "productAttributes[index].value"
                    }
                  })
                : _vm._e(),
              _vm._v(" "),
              _c(
                "span",
                {
                  directives: [
                    {
                      name: "show",
                      rawName: "v-show",
                      value: _vm.errors.has(itemAttribute.name),
                      expression: "errors.has(itemAttribute.name)"
                    }
                  ],
                  staticClass: "text-danger text-sm"
                },
                [_vm._v(_vm._s(_vm.errors.first(itemAttribute.name)))]
              )
            ],
            1
          )
        })
      ],
      2
    )
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/components/product-master/ProductCatalogSelection.vue?vue&type=template&id=2d33e1f3&":
/*!*********************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/components/product-master/ProductCatalogSelection.vue?vue&type=template&id=2d33e1f3& ***!
  \*********************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("fragment", [
    _c("div", { staticClass: "mt-6 flex items-center justify-between px-6" }, [
      _c("h4", [_vm._v("SELECT CATALOG")])
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "p-6" }, [
      _vm.itemCatalogs.length === 0
        ? _c("h3", [
            _vm._v("\n      No Catalogs are attached to this item\n    ")
          ])
        : _vm._e(),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "demo-alignment" },
        _vm._l(_vm.itemCatalogs, function(catalog, indextr) {
          return _c(
            "vs-chip",
            {
              key: indextr,
              staticStyle: { cursor: "pointer !important" },
              attrs: { color: "primary" }
            },
            [
              _c(
                "span",
                {
                  on: {
                    click: function($event) {
                      return _vm.continueToNextStep(catalog)
                    }
                  }
                },
                [_vm._v("\n          " + _vm._s(catalog.name) + "\n        ")]
              )
            ]
          )
        }),
        1
      )
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/components/product-master/ProductDetails.vue?vue&type=template&id=fa58491c&":
/*!************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/components/product-master/ProductDetails.vue?vue&type=template&id=fa58491c& ***!
  \************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("fragment", [
    _c("div", { staticClass: "mt-6 flex items-center justify-between px-6" }, [
      _c("h4", [_vm._v("ENTER PRODUCT DETAIL")])
    ]),
    _vm._v(" "),
    _c(
      "div",
      { staticClass: "p-6" },
      [
        _c("vs-input", {
          directives: [
            {
              name: "validate",
              rawName: "v-validate",
              value: "required",
              expression: "'required'"
            }
          ],
          staticClass: "mt-5 w-full",
          attrs: {
            label: "Product Title",
            placeholder: "Enter Product Title",
            name: "title"
          },
          model: {
            value: _vm.values.title,
            callback: function($$v) {
              _vm.$set(_vm.values, "title", $$v)
            },
            expression: "values.title"
          }
        }),
        _vm._v(" "),
        _c("single-picture", {
          attrs: {
            onChange: _vm.onChange,
            attribute: { name: "Product Image" },
            prodAttribute: _vm.imageSrc
          }
        }),
        _vm._v(" "),
        _c("single-picture", {
          attrs: {
            onChange: _vm.onChange,
            attribute: { name: "Product Mirror Image" },
            prodAttribute: _vm.mirrorImageSrc
          }
        }),
        _vm._v(" "),
        _c("label", { staticClass: "vs-input--label mt-5" }, [
          _vm._v("On Sale")
        ]),
        _vm._v(" "),
        _c("ul", { staticClass: "centerx w-full" }, [
          _c(
            "li",
            [
              _c(
                "vs-radio",
                {
                  attrs: { "vs-value": "true" },
                  model: {
                    value: _vm.values.on_sale,
                    callback: function($$v) {
                      _vm.$set(_vm.values, "on_sale", $$v)
                    },
                    expression: "values.on_sale"
                  }
                },
                [_vm._v("Yes")]
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "li",
            [
              _c(
                "vs-radio",
                {
                  attrs: { "vs-value": "false" },
                  model: {
                    value: _vm.values.on_sale,
                    callback: function($$v) {
                      _vm.$set(_vm.values, "on_sale", $$v)
                    },
                    expression: "values.on_sale"
                  }
                },
                [_vm._v("No")]
              )
            ],
            1
          )
        ]),
        _vm._v(" "),
        _c("label", { staticClass: "vs-input--label mt-5" }, [
          _vm._v("New Product")
        ]),
        _vm._v(" "),
        _c("ul", { staticClass: "centerx w-full" }, [
          _c(
            "li",
            [
              _c(
                "vs-radio",
                {
                  attrs: { "vs-value": "true" },
                  model: {
                    value: _vm.values.new_product,
                    callback: function($$v) {
                      _vm.$set(_vm.values, "new_product", $$v)
                    },
                    expression: "values.new_product"
                  }
                },
                [_vm._v("Yes")]
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "li",
            [
              _c(
                "vs-radio",
                {
                  attrs: { "vs-value": "false" },
                  model: {
                    value: _vm.values.new_product,
                    callback: function($$v) {
                      _vm.$set(_vm.values, "new_product", $$v)
                    },
                    expression: "values.new_product"
                  }
                },
                [_vm._v("No")]
              )
            ],
            1
          )
        ]),
        _vm._v(" "),
        _c("label", { staticClass: "vs-input--label mt-5" }, [_vm._v("Price")]),
        _vm._v(" "),
        _c("vs-input-number", {
          directives: [
            {
              name: "validate",
              rawName: "v-validate",
              value: "required",
              expression: "'required'"
            }
          ],
          staticClass: "centrex w-full",
          attrs: { name: _vm.values.old_price, placeholder: "Enter Price" },
          model: {
            value: _vm.values.old_price,
            callback: function($$v) {
              _vm.$set(_vm.values, "old_price", $$v)
            },
            expression: "values.old_price"
          }
        }),
        _vm._v(" "),
        _c("label", { staticClass: "vs-input--label mt-5" }, [_vm._v("Offer")]),
        _vm._v(" "),
        _c("ul", { staticClass: "centerx w-full" }, [
          _c(
            "li",
            [
              _c(
                "vs-radio",
                {
                  attrs: { "vs-value": "true" },
                  model: {
                    value: _vm.values.offer,
                    callback: function($$v) {
                      _vm.$set(_vm.values, "offer", $$v)
                    },
                    expression: "values.offer"
                  }
                },
                [_vm._v("Yes")]
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "li",
            [
              _c(
                "vs-radio",
                {
                  attrs: { "vs-value": "false" },
                  model: {
                    value: _vm.values.offer,
                    callback: function($$v) {
                      _vm.$set(_vm.values, "offer", $$v)
                    },
                    expression: "values.offer"
                  }
                },
                [_vm._v("No")]
              )
            ],
            1
          )
        ]),
        _vm._v(" "),
        _vm.values.offer == "true"
          ? _c("label", { staticClass: "vs-input--label mt-5" }, [
              _vm._v("Offer Price")
            ])
          : _vm._e(),
        _vm._v(" "),
        _vm.values.offer == "true"
          ? _c("vs-input-number", {
              directives: [
                {
                  name: "validate",
                  rawName: "v-validate",
                  value: "required",
                  expression: "'required'"
                }
              ],
              staticClass: "centrex w-full",
              attrs: { name: "new_price", placeholder: "Enter New Price" },
              model: {
                value: _vm.values.new_price,
                callback: function($$v) {
                  _vm.$set(_vm.values, "new_price", $$v)
                },
                expression: "values.new_price"
              }
            })
          : _vm._e(),
        _vm._v(" "),
        _c("label", { staticClass: "vs-input--label mt-5" }, [
          _vm._v("Discount")
        ]),
        _vm._v(" "),
        _c("ul", { staticClass: "centerx w-full" }, [
          _c(
            "li",
            [
              _c(
                "vs-radio",
                {
                  attrs: { "vs-value": "true" },
                  model: {
                    value: _vm.values.discount,
                    callback: function($$v) {
                      _vm.$set(_vm.values, "discount", $$v)
                    },
                    expression: "values.discount"
                  }
                },
                [_vm._v("Yes")]
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "li",
            [
              _c(
                "vs-radio",
                {
                  attrs: { "vs-value": "false" },
                  model: {
                    value: _vm.values.discount,
                    callback: function($$v) {
                      _vm.$set(_vm.values, "discount", $$v)
                    },
                    expression: "values.discount"
                  }
                },
                [_vm._v("No")]
              )
            ],
            1
          )
        ]),
        _vm._v(" "),
        _vm.values.discount == "true"
          ? _c("label", { staticClass: "vs-input--label mt-5" }, [
              _vm._v("Discount Off")
            ])
          : _vm._e(),
        _vm._v(" "),
        _vm.values.discount == "true"
          ? _c("vs-input-number", {
              directives: [
                {
                  name: "validate",
                  rawName: "v-validate",
                  value: "required",
                  expression: "'required'"
                }
              ],
              staticClass: "centrex w-full",
              attrs: {
                name: "discount_off",
                placeholder: "Enter off Discount"
              },
              model: {
                value: _vm.values.discount_off,
                callback: function($$v) {
                  _vm.$set(_vm.values, "discount_off", $$v)
                },
                expression: "values.discount_off"
              }
            })
          : _vm._e(),
        _vm._v(" "),
        _c("vs-textarea", {
          directives: [
            {
              name: "validate",
              rawName: "v-validate",
              value: "required",
              expression: "'required'"
            }
          ],
          staticClass: "mt-3",
          attrs: {
            label: "Description",
            placeholder: "Description for Product"
          },
          model: {
            value: _vm.values.description,
            callback: function($$v) {
              _vm.$set(_vm.values, "description", $$v)
            },
            expression: "values.description"
          }
        })
      ],
      1
    )
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/components/product-master/ProductItemSelection.vue?vue&type=template&id=5bcc1f69&":
/*!******************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/components/product-master/ProductItemSelection.vue?vue&type=template&id=5bcc1f69& ***!
  \******************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("fragment", [
    _c("div", { staticClass: "mt-6 flex items-center justify-between px-6" }, [
      _c("h4", [_vm._v("SELECT ITEM")])
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "p-6" }, [
      _c(
        "div",
        { staticClass: "demo-alignment" },
        _vm._l(_vm.productItems, function(item, indextr) {
          return _c(
            "vs-chip",
            {
              key: indextr,
              staticStyle: { cursor: "pointer !important" },
              attrs: { color: "primary" }
            },
            [
              _c(
                "span",
                {
                  on: {
                    click: function($event) {
                      return _vm.continueToNextStep(item)
                    }
                  }
                },
                [_vm._v("\n          " + _vm._s(item.name) + "\n        ")]
              )
            ]
          )
        }),
        1
      )
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/components/product-master/SinglePicture.vue?vue&type=template&id=3e138247&":
/*!***********************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/components/product-master/SinglePicture.vue?vue&type=template&id=3e138247& ***!
  \***********************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("fragment", [
    _vm.productAttribute && _vm.productAttribute.value
      ? _c("div", [
          _c(
            "div",
            {
              staticClass:
                "img-container w-64 mx-auto flex items-center justify-center"
            },
            [
              _c("img", {
                staticClass: "responsive",
                attrs: { src: _vm.productAttribute.value, alt: "img" }
              })
            ]
          ),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "modify-img flex justify-between mt-5" },
            [
              _c("input", {
                ref: "updateImgInput",
                staticClass: "hidden",
                attrs: { type: "file", accept: "image/*" },
                on: { change: _vm.updateCurrImg }
              }),
              _vm._v(" "),
              _c(
                "vs-button",
                {
                  staticClass: "mr-4",
                  attrs: { type: "flat" },
                  on: {
                    click: function($event) {
                      return _vm.$refs.updateImgInput.click()
                    }
                  }
                },
                [_vm._v("Update " + _vm._s(_vm.attribute.name))]
              ),
              _vm._v(" "),
              _c(
                "vs-button",
                {
                  attrs: { type: "flat", color: "#999" },
                  on: {
                    click: function($event) {
                      _vm.productAttribute.value = null
                    }
                  }
                },
                [_vm._v("Remove " + _vm._s(_vm.attribute.name))]
              )
            ],
            1
          )
        ])
      : _vm._e(),
    _vm._v(" "),
    !_vm.productAttribute || !_vm.productAttribute.value
      ? _c(
          "div",
          { staticClass: "upload-img mt-5" },
          [
            _c("input", {
              ref: "uploadImgInput",
              staticClass: "hidden",
              attrs: { type: "file", accept: "image/*" },
              on: { change: _vm.updateCurrImg }
            }),
            _vm._v(" "),
            _c(
              "vs-button",
              {
                on: {
                  click: function($event) {
                    return _vm.$refs.uploadImgInput.click()
                  }
                }
              },
              [_vm._v("Upload " + _vm._s(_vm.attribute.name))]
            )
          ],
          1
        )
      : _vm._e()
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/product-master/ProductMaster.vue?vue&type=template&id=3d6405b7&":
/*!************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/pages/product-master/ProductMaster.vue?vue&type=template&id=3d6405b7& ***!
  \************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    {
      staticClass: "data-list-container",
      attrs: { id: "data-list-list-view" }
    },
    [
      _c("product-master-sidebar", {
        attrs: {
          isSidebarActive: _vm.addNewDataSidebar,
          data: _vm.sidebarData
        },
        on: { closeSidebar: _vm.toggleDataSidebar }
      }),
      _vm._v(" "),
      _c(
        "vs-table",
        {
          ref: "table",
          attrs: {
            multiple: "",
            pagination: "",
            "max-items": _vm.itemsPerPage,
            search: "",
            data: _vm.products
          },
          scopedSlots: _vm._u([
            {
              key: "default",
              fn: function(ref) {
                var data = ref.data
                return [
                  _c(
                    "tbody",
                    _vm._l(data, function(tr, indextr) {
                      return _c(
                        "vs-tr",
                        { key: indextr, class: tr.status, attrs: { data: tr } },
                        [
                          _c("vs-td", [
                            _c("p", { staticClass: "product-category" }, [
                              _vm._v(_vm._s(tr.id))
                            ])
                          ]),
                          _vm._v(" "),
                          _c("vs-td", [
                            _c(
                              "p",
                              {
                                staticClass: "product-name font-medium truncate"
                              },
                              [_vm._v(_vm._s(tr.item_id))]
                            )
                          ]),
                          _vm._v(" "),
                          _c("vs-td", [
                            _c("p", { staticClass: "product-category" }, [
                              _vm._v(_vm._s(tr.item_name))
                            ])
                          ]),
                          _vm._v(" "),
                          _c("vs-td", [
                            _c("p", { staticClass: "product-category" }, [
                              _vm._v(_vm._s(tr.catalog_name))
                            ])
                          ]),
                          _vm._v(" "),
                          _c(
                            "vs-td",
                            { staticClass: "whitespace-no-wrap" },
                            [
                              _c("feather-icon", {
                                attrs: {
                                  icon: "EditIcon",
                                  svgClasses:
                                    "w-5 h-5 hover:text-primary stroke-current"
                                },
                                on: {
                                  click: function($event) {
                                    $event.stopPropagation()
                                    return _vm.editData(tr)
                                  }
                                }
                              }),
                              _vm._v(" "),
                              _c("feather-icon", {
                                staticClass: "ml-2",
                                attrs: {
                                  icon: "TrashIcon",
                                  svgClasses:
                                    "w-5 h-5 hover:text-danger stroke-current"
                                },
                                on: {
                                  click: function($event) {
                                    $event.stopPropagation()
                                    return _vm.deleteData(tr.id)
                                  }
                                }
                              })
                            ],
                            1
                          )
                        ],
                        1
                      )
                    }),
                    1
                  )
                ]
              }
            }
          ]),
          model: {
            value: _vm.selected,
            callback: function($$v) {
              _vm.selected = $$v
            },
            expression: "selected"
          }
        },
        [
          _c(
            "div",
            {
              staticClass:
                "flex flex-wrap-reverse items-center flex-grow justify-between",
              attrs: { slot: "header" },
              slot: "header"
            },
            [
              _c(
                "div",
                {
                  staticClass:
                    "flex flex-wrap-reverse items-center data-list-btn-container"
                },
                [
                  _c(
                    "vs-dropdown",
                    {
                      staticClass: "dd-actions cursor-pointer mr-4 mb-4",
                      attrs: { "vs-trigger-click": "" }
                    },
                    [
                      _c(
                        "div",
                        {
                          staticClass:
                            "p-4 shadow-drop rounded-lg d-theme-dark-bg cursor-pointer flex items-center justify-center text-lg font-medium w-32 w-full"
                        },
                        [
                          _c("span", { staticClass: "mr-2" }, [
                            _vm._v("Actions")
                          ]),
                          _vm._v(" "),
                          _c("feather-icon", {
                            attrs: {
                              icon: "ChevronDownIcon",
                              svgClasses: "h-4 w-4"
                            }
                          })
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "vs-dropdown-menu",
                        [
                          _c("vs-dropdown-item", [
                            _c(
                              "span",
                              { staticClass: "flex items-center" },
                              [
                                _c("feather-icon", {
                                  staticClass: "mr-2",
                                  attrs: {
                                    icon: "TrashIcon",
                                    svgClasses: "h-4 w-4"
                                  }
                                }),
                                _vm._v(" "),
                                _c("span", [_vm._v("Delete")])
                              ],
                              1
                            )
                          ]),
                          _vm._v(" "),
                          _c(
                            "vs-dropdown-item",
                            { on: { click: _vm.toggleStatus } },
                            [
                              _c(
                                "span",
                                { staticClass: "flex items-center" },
                                [
                                  _c("feather-icon", {
                                    staticClass: "mr-2",
                                    attrs: {
                                      icon: "ToggleLeftIcon",
                                      svgClasses: "h-4 w-4"
                                    }
                                  }),
                                  _vm._v(" "),
                                  _c("span", [_vm._v("Toggle Status")])
                                ],
                                1
                              )
                            ]
                          )
                        ],
                        1
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "div",
                    {
                      staticClass:
                        "btn-add-new p-3 mb-4 mr-4 rounded-lg cursor-pointer flex items-center justify-center text-lg font-medium text-base text-primary border border-solid border-primary",
                      on: { click: _vm.addNewData }
                    },
                    [
                      _c("feather-icon", {
                        attrs: { icon: "PlusIcon", svgClasses: "h-4 w-4" }
                      }),
                      _vm._v(" "),
                      _c(
                        "span",
                        { staticClass: "ml-2 text-base text-primary" },
                        [_vm._v("Add New")]
                      )
                    ],
                    1
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "vs-dropdown",
                {
                  staticClass:
                    "cursor-pointer mb-4 mr-4 items-per-page-handler",
                  attrs: { "vs-trigger-click": "" }
                },
                [
                  _c(
                    "div",
                    {
                      staticClass:
                        "p-4 border border-solid d-theme-border-grey-light rounded-full d-theme-dark-bg cursor-pointer flex items-center justify-between font-medium"
                    },
                    [
                      _c("span", { staticClass: "mr-2" }, [
                        _vm._v(
                          _vm._s(
                            _vm.currentPage * _vm.itemsPerPage -
                              (_vm.itemsPerPage - 1)
                          ) +
                            " - " +
                            _vm._s(
                              _vm.products.length -
                                _vm.currentPage * _vm.itemsPerPage >
                                0
                                ? _vm.currentPage * _vm.itemsPerPage
                                : _vm.products.length
                            ) +
                            " of " +
                            _vm._s(_vm.queriedItems)
                        )
                      ]),
                      _vm._v(" "),
                      _c("feather-icon", {
                        attrs: {
                          icon: "ChevronDownIcon",
                          svgClasses: "h-4 w-4"
                        }
                      })
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "vs-dropdown-menu",
                    [
                      _c(
                        "vs-dropdown-item",
                        {
                          on: {
                            click: function($event) {
                              _vm.itemsPerPage = 4
                            }
                          }
                        },
                        [_c("span", [_vm._v("4")])]
                      ),
                      _vm._v(" "),
                      _c(
                        "vs-dropdown-item",
                        {
                          on: {
                            click: function($event) {
                              _vm.itemsPerPage = 10
                            }
                          }
                        },
                        [_c("span", [_vm._v("10")])]
                      ),
                      _vm._v(" "),
                      _c(
                        "vs-dropdown-item",
                        {
                          on: {
                            click: function($event) {
                              _vm.itemsPerPage = 15
                            }
                          }
                        },
                        [_c("span", [_vm._v("15")])]
                      ),
                      _vm._v(" "),
                      _c(
                        "vs-dropdown-item",
                        {
                          on: {
                            click: function($event) {
                              _vm.itemsPerPage = 20
                            }
                          }
                        },
                        [_c("span", [_vm._v("20")])]
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "template",
            { slot: "thead" },
            [
              _c("vs-th", { attrs: { "sort-key": "id" } }, [_vm._v("ID")]),
              _vm._v(" "),
              _c("vs-th", { attrs: { "sort-key": "item_id" } }, [
                _vm._v("Item ID")
              ]),
              _vm._v(" "),
              _c("vs-th", { attrs: { "sort-key": "item_name" } }, [
                _vm._v("Item Name")
              ]),
              _vm._v(" "),
              _c("vs-th", { attrs: { "sort-key": "catalog_name" } }, [
                _vm._v("Catalog Name")
              ]),
              _vm._v(" "),
              _c("vs-th", [_vm._v("Action")])
            ],
            1
          )
        ],
        2
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/product-master/ProductMasterSidebar.vue?vue&type=template&id=fa7dd856&scoped=true&":
/*!*******************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/pages/product-master/ProductMasterSidebar.vue?vue&type=template&id=fa7dd856&scoped=true& ***!
  \*******************************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "vs-sidebar",
    {
      staticClass: "add-new-data-sidebar items-no-padding",
      attrs: {
        "click-not-close": "",
        "position-right": "",
        parent: "body",
        "default-index": "1",
        color: "primary",
        spacer: ""
      },
      model: {
        value: _vm.isSidebarActiveLocal,
        callback: function($$v) {
          _vm.isSidebarActiveLocal = $$v
        },
        expression: "isSidebarActiveLocal"
      }
    },
    [
      _c(
        "div",
        { staticClass: "mt-6 flex items-center justify-between px-6" },
        [
          _c("h4", [
            _vm._v(
              "\n      " +
                _vm._s(
                  Object.entries(this.data).length === 0 ? "ADD " : "UPDATE"
                ) +
                "PRODUCT\n    "
            )
          ]),
          _vm._v(" "),
          _c("feather-icon", {
            staticClass: "cursor-pointer",
            attrs: { icon: "XIcon" },
            on: {
              click: function($event) {
                $event.stopPropagation()
                _vm.isSidebarActiveLocal = false
              }
            }
          })
        ],
        1
      ),
      _vm._v(" "),
      _c("vs-divider", { staticClass: "mb-0" }),
      _vm._v(" "),
      _c(
        "VuePerfectScrollbar",
        {
          key: _vm.$vs.rtl,
          staticClass: "scroll-area--data-list-add-new",
          attrs: { settings: _vm.settings }
        },
        [
          _vm.step === 1
            ? _c("product-item-selection", {
                attrs: {
                  productItems: _vm.productItems,
                  nextStep: _vm.nextStep,
                  isSidebarActiveLocal: _vm.isSidebarActiveLocal,
                  values: _vm.values
                }
              })
            : _vm._e(),
          _vm._v(" "),
          _vm.step === 2
            ? _c("product-catalog-selection", {
                attrs: {
                  itemCatalogs: _vm.itemCatalogs,
                  nextStep: _vm.nextStep,
                  isSidebarActiveLocal: _vm.isSidebarActiveLocal,
                  values: _vm.values
                }
              })
            : _vm._e(),
          _vm._v(" "),
          _vm.step === 3
            ? _c("product-details", {
                attrs: {
                  isSidebarActiveLocal: _vm.isSidebarActiveLocal,
                  values: _vm.values
                }
              })
            : _vm._e(),
          _vm._v(" "),
          _vm.step === 3
            ? _c("product-attributes-values", {
                attrs: {
                  itemAttributes: _vm.itemAttributes,
                  isSidebarActiveLocal: _vm.isSidebarActiveLocal,
                  values: _vm.values,
                  prodAttributes: _vm.values.productAttributes
                }
              })
            : _vm._e()
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "div",
        {
          staticClass: "flex flex-wrap items-center p-6",
          attrs: { slot: "footer" },
          slot: "footer"
        },
        [
          _c(
            "vs-button",
            {
              staticClass: "mr-6",
              attrs: { disabled: !_vm.isFormValid },
              on: { click: _vm.submitData }
            },
            [_vm._v("Submit")]
          ),
          _vm._v(" "),
          _c(
            "vs-button",
            {
              attrs: { type: "border", color: "danger" },
              on: {
                click: function($event) {
                  _vm.step === 2 || _vm.step === 3
                    ? _vm.prevStep()
                    : (_vm.isSidebarActiveLocal = false)
                }
              }
            },
            [
              _vm._v(
                _vm._s(_vm.step === 2 || _vm.step === 3 ? "Back" : "Cancel")
              )
            ]
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/src/components/product-master/MultiplePictures.vue":
/*!*************************************************************************!*\
  !*** ./resources/js/src/components/product-master/MultiplePictures.vue ***!
  \*************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _MultiplePictures_vue_vue_type_template_id_494237c4___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./MultiplePictures.vue?vue&type=template&id=494237c4& */ "./resources/js/src/components/product-master/MultiplePictures.vue?vue&type=template&id=494237c4&");
/* harmony import */ var _MultiplePictures_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./MultiplePictures.vue?vue&type=script&lang=js& */ "./resources/js/src/components/product-master/MultiplePictures.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _MultiplePictures_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _MultiplePictures_vue_vue_type_template_id_494237c4___WEBPACK_IMPORTED_MODULE_0__["render"],
  _MultiplePictures_vue_vue_type_template_id_494237c4___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/src/components/product-master/MultiplePictures.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/src/components/product-master/MultiplePictures.vue?vue&type=script&lang=js&":
/*!**************************************************************************************************!*\
  !*** ./resources/js/src/components/product-master/MultiplePictures.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_MultiplePictures_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./MultiplePictures.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/components/product-master/MultiplePictures.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_MultiplePictures_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/src/components/product-master/MultiplePictures.vue?vue&type=template&id=494237c4&":
/*!********************************************************************************************************!*\
  !*** ./resources/js/src/components/product-master/MultiplePictures.vue?vue&type=template&id=494237c4& ***!
  \********************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_MultiplePictures_vue_vue_type_template_id_494237c4___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./MultiplePictures.vue?vue&type=template&id=494237c4& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/components/product-master/MultiplePictures.vue?vue&type=template&id=494237c4&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_MultiplePictures_vue_vue_type_template_id_494237c4___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_MultiplePictures_vue_vue_type_template_id_494237c4___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/src/components/product-master/ProductAttributesValues.vue":
/*!********************************************************************************!*\
  !*** ./resources/js/src/components/product-master/ProductAttributesValues.vue ***!
  \********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ProductAttributesValues_vue_vue_type_template_id_b52c524e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ProductAttributesValues.vue?vue&type=template&id=b52c524e& */ "./resources/js/src/components/product-master/ProductAttributesValues.vue?vue&type=template&id=b52c524e&");
/* harmony import */ var _ProductAttributesValues_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ProductAttributesValues.vue?vue&type=script&lang=js& */ "./resources/js/src/components/product-master/ProductAttributesValues.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _ProductAttributesValues_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ProductAttributesValues_vue_vue_type_template_id_b52c524e___WEBPACK_IMPORTED_MODULE_0__["render"],
  _ProductAttributesValues_vue_vue_type_template_id_b52c524e___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/src/components/product-master/ProductAttributesValues.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/src/components/product-master/ProductAttributesValues.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************************!*\
  !*** ./resources/js/src/components/product-master/ProductAttributesValues.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ProductAttributesValues_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./ProductAttributesValues.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/components/product-master/ProductAttributesValues.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ProductAttributesValues_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/src/components/product-master/ProductAttributesValues.vue?vue&type=template&id=b52c524e&":
/*!***************************************************************************************************************!*\
  !*** ./resources/js/src/components/product-master/ProductAttributesValues.vue?vue&type=template&id=b52c524e& ***!
  \***************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ProductAttributesValues_vue_vue_type_template_id_b52c524e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./ProductAttributesValues.vue?vue&type=template&id=b52c524e& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/components/product-master/ProductAttributesValues.vue?vue&type=template&id=b52c524e&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ProductAttributesValues_vue_vue_type_template_id_b52c524e___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ProductAttributesValues_vue_vue_type_template_id_b52c524e___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/src/components/product-master/ProductCatalogSelection.vue":
/*!********************************************************************************!*\
  !*** ./resources/js/src/components/product-master/ProductCatalogSelection.vue ***!
  \********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ProductCatalogSelection_vue_vue_type_template_id_2d33e1f3___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ProductCatalogSelection.vue?vue&type=template&id=2d33e1f3& */ "./resources/js/src/components/product-master/ProductCatalogSelection.vue?vue&type=template&id=2d33e1f3&");
/* harmony import */ var _ProductCatalogSelection_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ProductCatalogSelection.vue?vue&type=script&lang=js& */ "./resources/js/src/components/product-master/ProductCatalogSelection.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _ProductCatalogSelection_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ProductCatalogSelection_vue_vue_type_template_id_2d33e1f3___WEBPACK_IMPORTED_MODULE_0__["render"],
  _ProductCatalogSelection_vue_vue_type_template_id_2d33e1f3___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/src/components/product-master/ProductCatalogSelection.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/src/components/product-master/ProductCatalogSelection.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************************!*\
  !*** ./resources/js/src/components/product-master/ProductCatalogSelection.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ProductCatalogSelection_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./ProductCatalogSelection.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/components/product-master/ProductCatalogSelection.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ProductCatalogSelection_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/src/components/product-master/ProductCatalogSelection.vue?vue&type=template&id=2d33e1f3&":
/*!***************************************************************************************************************!*\
  !*** ./resources/js/src/components/product-master/ProductCatalogSelection.vue?vue&type=template&id=2d33e1f3& ***!
  \***************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ProductCatalogSelection_vue_vue_type_template_id_2d33e1f3___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./ProductCatalogSelection.vue?vue&type=template&id=2d33e1f3& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/components/product-master/ProductCatalogSelection.vue?vue&type=template&id=2d33e1f3&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ProductCatalogSelection_vue_vue_type_template_id_2d33e1f3___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ProductCatalogSelection_vue_vue_type_template_id_2d33e1f3___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/src/components/product-master/ProductDetails.vue":
/*!***********************************************************************!*\
  !*** ./resources/js/src/components/product-master/ProductDetails.vue ***!
  \***********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ProductDetails_vue_vue_type_template_id_fa58491c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ProductDetails.vue?vue&type=template&id=fa58491c& */ "./resources/js/src/components/product-master/ProductDetails.vue?vue&type=template&id=fa58491c&");
/* harmony import */ var _ProductDetails_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ProductDetails.vue?vue&type=script&lang=js& */ "./resources/js/src/components/product-master/ProductDetails.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _ProductDetails_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ProductDetails_vue_vue_type_template_id_fa58491c___WEBPACK_IMPORTED_MODULE_0__["render"],
  _ProductDetails_vue_vue_type_template_id_fa58491c___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/src/components/product-master/ProductDetails.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/src/components/product-master/ProductDetails.vue?vue&type=script&lang=js&":
/*!************************************************************************************************!*\
  !*** ./resources/js/src/components/product-master/ProductDetails.vue?vue&type=script&lang=js& ***!
  \************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ProductDetails_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./ProductDetails.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/components/product-master/ProductDetails.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ProductDetails_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/src/components/product-master/ProductDetails.vue?vue&type=template&id=fa58491c&":
/*!******************************************************************************************************!*\
  !*** ./resources/js/src/components/product-master/ProductDetails.vue?vue&type=template&id=fa58491c& ***!
  \******************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ProductDetails_vue_vue_type_template_id_fa58491c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./ProductDetails.vue?vue&type=template&id=fa58491c& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/components/product-master/ProductDetails.vue?vue&type=template&id=fa58491c&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ProductDetails_vue_vue_type_template_id_fa58491c___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ProductDetails_vue_vue_type_template_id_fa58491c___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/src/components/product-master/ProductItemSelection.vue":
/*!*****************************************************************************!*\
  !*** ./resources/js/src/components/product-master/ProductItemSelection.vue ***!
  \*****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ProductItemSelection_vue_vue_type_template_id_5bcc1f69___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ProductItemSelection.vue?vue&type=template&id=5bcc1f69& */ "./resources/js/src/components/product-master/ProductItemSelection.vue?vue&type=template&id=5bcc1f69&");
/* harmony import */ var _ProductItemSelection_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ProductItemSelection.vue?vue&type=script&lang=js& */ "./resources/js/src/components/product-master/ProductItemSelection.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _ProductItemSelection_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ProductItemSelection_vue_vue_type_template_id_5bcc1f69___WEBPACK_IMPORTED_MODULE_0__["render"],
  _ProductItemSelection_vue_vue_type_template_id_5bcc1f69___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/src/components/product-master/ProductItemSelection.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/src/components/product-master/ProductItemSelection.vue?vue&type=script&lang=js&":
/*!******************************************************************************************************!*\
  !*** ./resources/js/src/components/product-master/ProductItemSelection.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ProductItemSelection_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./ProductItemSelection.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/components/product-master/ProductItemSelection.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ProductItemSelection_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/src/components/product-master/ProductItemSelection.vue?vue&type=template&id=5bcc1f69&":
/*!************************************************************************************************************!*\
  !*** ./resources/js/src/components/product-master/ProductItemSelection.vue?vue&type=template&id=5bcc1f69& ***!
  \************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ProductItemSelection_vue_vue_type_template_id_5bcc1f69___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./ProductItemSelection.vue?vue&type=template&id=5bcc1f69& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/components/product-master/ProductItemSelection.vue?vue&type=template&id=5bcc1f69&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ProductItemSelection_vue_vue_type_template_id_5bcc1f69___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ProductItemSelection_vue_vue_type_template_id_5bcc1f69___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/src/components/product-master/SinglePicture.vue":
/*!**********************************************************************!*\
  !*** ./resources/js/src/components/product-master/SinglePicture.vue ***!
  \**********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _SinglePicture_vue_vue_type_template_id_3e138247___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./SinglePicture.vue?vue&type=template&id=3e138247& */ "./resources/js/src/components/product-master/SinglePicture.vue?vue&type=template&id=3e138247&");
/* harmony import */ var _SinglePicture_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./SinglePicture.vue?vue&type=script&lang=js& */ "./resources/js/src/components/product-master/SinglePicture.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _SinglePicture_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _SinglePicture_vue_vue_type_template_id_3e138247___WEBPACK_IMPORTED_MODULE_0__["render"],
  _SinglePicture_vue_vue_type_template_id_3e138247___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/src/components/product-master/SinglePicture.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/src/components/product-master/SinglePicture.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************!*\
  !*** ./resources/js/src/components/product-master/SinglePicture.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_SinglePicture_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./SinglePicture.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/components/product-master/SinglePicture.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_SinglePicture_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/src/components/product-master/SinglePicture.vue?vue&type=template&id=3e138247&":
/*!*****************************************************************************************************!*\
  !*** ./resources/js/src/components/product-master/SinglePicture.vue?vue&type=template&id=3e138247& ***!
  \*****************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_SinglePicture_vue_vue_type_template_id_3e138247___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./SinglePicture.vue?vue&type=template&id=3e138247& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/components/product-master/SinglePicture.vue?vue&type=template&id=3e138247&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_SinglePicture_vue_vue_type_template_id_3e138247___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_SinglePicture_vue_vue_type_template_id_3e138247___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/src/store/productMaster/moduleProductMaster.js":
/*!*********************************************************************!*\
  !*** ./resources/js/src/store/productMaster/moduleProductMaster.js ***!
  \*********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _moduleProductMasterState_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./moduleProductMasterState.js */ "./resources/js/src/store/productMaster/moduleProductMasterState.js");
/* harmony import */ var _moduleProductMasterMutations_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./moduleProductMasterMutations.js */ "./resources/js/src/store/productMaster/moduleProductMasterMutations.js");
/* harmony import */ var _moduleProductMasterActions_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./moduleProductMasterActions.js */ "./resources/js/src/store/productMaster/moduleProductMasterActions.js");
/* harmony import */ var _moduleProductMasterGetters_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./moduleProductMasterGetters.js */ "./resources/js/src/store/productMaster/moduleProductMasterGetters.js");
/*=========================================================================================
  File Name: moduleProductMaster.js
  Description: ProductMaster Module
  ----------------------------------------------------------------------------------------
  Item ZainFit Vue Laravel Admin Panel Developed By RigRex
  Author: RigRex - Ahmad
  Author URL: https://www.rigrex.com
==========================================================================================*/




/* harmony default export */ __webpack_exports__["default"] = ({
  isRegistered: false,
  namespaced: true,
  state: _moduleProductMasterState_js__WEBPACK_IMPORTED_MODULE_0__["default"],
  mutations: _moduleProductMasterMutations_js__WEBPACK_IMPORTED_MODULE_1__["default"],
  actions: _moduleProductMasterActions_js__WEBPACK_IMPORTED_MODULE_2__["default"],
  getters: _moduleProductMasterGetters_js__WEBPACK_IMPORTED_MODULE_3__["default"]
});

/***/ }),

/***/ "./resources/js/src/store/productMaster/moduleProductMasterActions.js":
/*!****************************************************************************!*\
  !*** ./resources/js/src/store/productMaster/moduleProductMasterActions.js ***!
  \****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _axios_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @/axios.js */ "./resources/js/src/axios.js");
/*=========================================================================================
  File Name: moduleProductMasterActions.js
  Description: ProductMaster Module
  ----------------------------------------------------------------------------------------
  ProductMaster ZainFit Vue Laravel Admin Panel Developed By RigRex
  Author: RigRex - Ahmad
  Author URL: https://www.rigrex.com
==========================================================================================*/

/* harmony default export */ __webpack_exports__["default"] = ({
  // ADD PRODUCT
  addProduct: function addProduct(context, product) {
    _axios_js__WEBPACK_IMPORTED_MODULE_0__["default"].defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');
    return new Promise(function (resolve, reject) {
      _axios_js__WEBPACK_IMPORTED_MODULE_0__["default"].post("/api/admin/product-master/add-product", product, {
        headers: {
          'Content-Type': 'multipart/form-data'
        }
      }).then(function (response) {
        console.log(response.data.product);
        context.commit("ADD_PRODUCT", Object.assign(response.data.product, {
          id: response.data.id
        }));
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  //FETCH PRODUCTS
  fetchProducts: function fetchProducts(context) {
    _axios_js__WEBPACK_IMPORTED_MODULE_0__["default"].defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');
    return new Promise(function (resolve, reject) {
      _axios_js__WEBPACK_IMPORTED_MODULE_0__["default"].get("/api/admin/product-master/get-products").then(function (response) {
        // console.log(response);
        context.commit("SET_PRODUCTS", response.data);
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  //FETCH CATALOGS
  fetchCatalogs: function fetchCatalogs(context) {
    _axios_js__WEBPACK_IMPORTED_MODULE_0__["default"].defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');
    return new Promise(function (resolve, reject) {
      _axios_js__WEBPACK_IMPORTED_MODULE_0__["default"].get("/api/admin/product-master/get-catalogs").then(function (response) {
        // console.log(response);
        context.commit("SET_PRODUCT_CATALOGS", response.data);
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  //FETCH ITEMS
  fetchItems: function fetchItems(context) {
    _axios_js__WEBPACK_IMPORTED_MODULE_0__["default"].defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');
    return new Promise(function (resolve, reject) {
      _axios_js__WEBPACK_IMPORTED_MODULE_0__["default"].get("/api/admin/product-master/get-items").then(function (response) {
        // console.log(response);
        context.commit("SET_PRODUCT_ITEMS", response.data);
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  //FETCH ITEM CATALOGS
  fetchItemCatalogs: function fetchItemCatalogs(context, itemId) {
    _axios_js__WEBPACK_IMPORTED_MODULE_0__["default"].defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');
    return new Promise(function (resolve, reject) {
      _axios_js__WEBPACK_IMPORTED_MODULE_0__["default"].get("/api/admin/product-master/get-item-catalogs/".concat(itemId)).then(function (response) {
        // console.log(response);
        context.commit("SET_PRODUCT_ITEM_CATALOGS", response.data);
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  //FETCH ITEM ATTRIBUTES
  fetchItemAttributes: function fetchItemAttributes(context, itemId) {
    _axios_js__WEBPACK_IMPORTED_MODULE_0__["default"].defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');
    return new Promise(function (resolve, reject) {
      _axios_js__WEBPACK_IMPORTED_MODULE_0__["default"].get("/api/admin/product-master/get-item-attributes/".concat(itemId)).then(function (response) {
        // console.log(response);
        context.commit("SET_PRODUCT_ITEM_ATTRIBUTES", response.data);
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  // UPDATE PRODUCT
  updateProduct: function updateProduct(context, product) {
    _axios_js__WEBPACK_IMPORTED_MODULE_0__["default"].defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');
    return new Promise(function (resolve, reject) {
      _axios_js__WEBPACK_IMPORTED_MODULE_0__["default"].post("/api/admin/product-master/update-product/".concat(product.id), {
        product: product
      }).then(function (response) {
        context.commit("UPDATE_PRODUCT", response.data.product);
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  //REMOVE PRODUCT
  removeProduct: function removeProduct(context, productId) {
    _axios_js__WEBPACK_IMPORTED_MODULE_0__["default"].defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');
    return new Promise(function (resolve, reject) {
      _axios_js__WEBPACK_IMPORTED_MODULE_0__["default"].delete("/api/admin/product-master/remove-product/".concat(productId)).then(function (response) {
        context.commit("REMOVE_PRODUCT", productId);
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  toggleStatus: function toggleStatus(context, data) {
    _axios_js__WEBPACK_IMPORTED_MODULE_0__["default"].defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');
    return new Promise(function (resolve, reject) {
      _axios_js__WEBPACK_IMPORTED_MODULE_0__["default"].post("/api/admin/product-master/status-product", data).then(function (response) {
        console.log(response.data.data);
        context.commit('CHANGE_STATUS', response.data.data);
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  }
});

/***/ }),

/***/ "./resources/js/src/store/productMaster/moduleProductMasterGetters.js":
/*!****************************************************************************!*\
  !*** ./resources/js/src/store/productMaster/moduleProductMasterGetters.js ***!
  \****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/*=========================================================================================
  File Name: moduleCatalogActions.js
  Description: Catalog Module
  ----------------------------------------------------------------------------------------
  Item ZainFit Vue Laravel Admin Panel Developed By RigRex
  Author: RigRex - Ahmad
  Author URL: https://www.rigrex.com
==========================================================================================*/
/* harmony default export */ __webpack_exports__["default"] = ({// getItem: state => (productId) => state.products.find((product) => product.id == productId),
});

/***/ }),

/***/ "./resources/js/src/store/productMaster/moduleProductMasterMutations.js":
/*!******************************************************************************!*\
  !*** ./resources/js/src/store/productMaster/moduleProductMasterMutations.js ***!
  \******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/*=========================================================================================
  File Name: moduleCatalogMutations.js
  Description: Catalog Module
  ----------------------------------------------------------------------------------------
  Item ZainFit Vue Laravel Admin Panel Developed By RigRex
  Author: RigRex - Ahmad
  Author URL: https://www.rigrex.com
==========================================================================================*/
/* harmony default export */ __webpack_exports__["default"] = ({
  ADD_PRODUCT: function ADD_PRODUCT(state, product) {
    state.products.unshift(product);
  },
  SET_PRODUCTS: function SET_PRODUCTS(state, products) {
    //   console.log(products);
    state.products = products.products;
  },
  SET_PRODUCT_ITEMS: function SET_PRODUCT_ITEMS(state, items) {
    //   console.log(products);
    state.productItems = items.items;
  },
  SET_PRODUCT_CATALOGS: function SET_PRODUCT_CATALOGS(state, catalogs) {
    //   console.log(products);
    state.productCatalogs = catalogs.catalogs;
  },
  SET_PRODUCT_ITEM_CATALOGS: function SET_PRODUCT_ITEM_CATALOGS(state, data) {
    //   console.log(products);
    state.itemCatalogs = data.itemCatalogs;
  },
  SET_PRODUCT_ITEM_ATTRIBUTES: function SET_PRODUCT_ITEM_ATTRIBUTES(state, data) {
    console.log(data);
    state.itemAttributes = data.itemAttributes;
  },
  UPDATE_PRODUCT: function UPDATE_PRODUCT(state, product) {
    var productIndex = state.products.findIndex(function (c) {
      return c.id == product.id;
    });
    Object.assign(state.products[productIndex], product);
  },
  REMOVE_PRODUCT: function REMOVE_PRODUCT(state, productId) {
    var productIndex = state.products.findIndex(function (c) {
      return c.id == productId;
    });
    state.products.splice(productIndex, 1);
  },
  CHANGE_STATUS: function CHANGE_STATUS(state, data) {
    data.forEach(function (element) {
      var itemIndex = state.products.findIndex(function (i) {
        return i.id == element.id;
      });
      var status = element.status == "inactive" ? "active" : "inactive";
      state.products[itemIndex].status = status;
    });
  }
});

/***/ }),

/***/ "./resources/js/src/store/productMaster/moduleProductMasterState.js":
/*!**************************************************************************!*\
  !*** ./resources/js/src/store/productMaster/moduleProductMasterState.js ***!
  \**************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/*=========================================================================================
  File Name: moduleAttributeMasterState.js
  Description: Attribute Module
  ----------------------------------------------------------------------------------------
  Item ZainFit Vue Laravel Admin Panel Developed By RigRex
  Author: RigRex - Ahmad
  Author URL: https://www.rigrex.com
==========================================================================================*/
/* harmony default export */ __webpack_exports__["default"] = ({
  products: [],
  productCatalogs: [],
  productItems: [],
  itemCatalogs: [],
  itemAttributes: []
});

/***/ }),

/***/ "./resources/js/src/views/pages/product-master/ProductMaster.vue":
/*!***********************************************************************!*\
  !*** ./resources/js/src/views/pages/product-master/ProductMaster.vue ***!
  \***********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ProductMaster_vue_vue_type_template_id_3d6405b7___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ProductMaster.vue?vue&type=template&id=3d6405b7& */ "./resources/js/src/views/pages/product-master/ProductMaster.vue?vue&type=template&id=3d6405b7&");
/* harmony import */ var _ProductMaster_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ProductMaster.vue?vue&type=script&lang=js& */ "./resources/js/src/views/pages/product-master/ProductMaster.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _ProductMaster_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./ProductMaster.vue?vue&type=style&index=0&lang=scss& */ "./resources/js/src/views/pages/product-master/ProductMaster.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _ProductMaster_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ProductMaster_vue_vue_type_template_id_3d6405b7___WEBPACK_IMPORTED_MODULE_0__["render"],
  _ProductMaster_vue_vue_type_template_id_3d6405b7___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/src/views/pages/product-master/ProductMaster.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/src/views/pages/product-master/ProductMaster.vue?vue&type=script&lang=js&":
/*!************************************************************************************************!*\
  !*** ./resources/js/src/views/pages/product-master/ProductMaster.vue?vue&type=script&lang=js& ***!
  \************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ProductMaster_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./ProductMaster.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/product-master/ProductMaster.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ProductMaster_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/src/views/pages/product-master/ProductMaster.vue?vue&type=style&index=0&lang=scss&":
/*!*********************************************************************************************************!*\
  !*** ./resources/js/src/views/pages/product-master/ProductMaster.vue?vue&type=style&index=0&lang=scss& ***!
  \*********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_ProductMaster_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/style-loader!../../../../../../node_modules/css-loader!../../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../../node_modules/postcss-loader/src??ref--8-2!../../../../../../node_modules/sass-loader/dist/cjs.js??ref--8-3!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./ProductMaster.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/product-master/ProductMaster.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_ProductMaster_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_ProductMaster_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_ProductMaster_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_ProductMaster_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_ProductMaster_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/src/views/pages/product-master/ProductMaster.vue?vue&type=template&id=3d6405b7&":
/*!******************************************************************************************************!*\
  !*** ./resources/js/src/views/pages/product-master/ProductMaster.vue?vue&type=template&id=3d6405b7& ***!
  \******************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ProductMaster_vue_vue_type_template_id_3d6405b7___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./ProductMaster.vue?vue&type=template&id=3d6405b7& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/product-master/ProductMaster.vue?vue&type=template&id=3d6405b7&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ProductMaster_vue_vue_type_template_id_3d6405b7___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ProductMaster_vue_vue_type_template_id_3d6405b7___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/src/views/pages/product-master/ProductMasterSidebar.vue":
/*!******************************************************************************!*\
  !*** ./resources/js/src/views/pages/product-master/ProductMasterSidebar.vue ***!
  \******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ProductMasterSidebar_vue_vue_type_template_id_fa7dd856_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ProductMasterSidebar.vue?vue&type=template&id=fa7dd856&scoped=true& */ "./resources/js/src/views/pages/product-master/ProductMasterSidebar.vue?vue&type=template&id=fa7dd856&scoped=true&");
/* harmony import */ var _ProductMasterSidebar_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ProductMasterSidebar.vue?vue&type=script&lang=js& */ "./resources/js/src/views/pages/product-master/ProductMasterSidebar.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _ProductMasterSidebar_vue_vue_type_style_index_0_id_fa7dd856_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./ProductMasterSidebar.vue?vue&type=style&index=0&id=fa7dd856&lang=scss&scoped=true& */ "./resources/js/src/views/pages/product-master/ProductMasterSidebar.vue?vue&type=style&index=0&id=fa7dd856&lang=scss&scoped=true&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _ProductMasterSidebar_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ProductMasterSidebar_vue_vue_type_template_id_fa7dd856_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _ProductMasterSidebar_vue_vue_type_template_id_fa7dd856_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "fa7dd856",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/src/views/pages/product-master/ProductMasterSidebar.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/src/views/pages/product-master/ProductMasterSidebar.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************************!*\
  !*** ./resources/js/src/views/pages/product-master/ProductMasterSidebar.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ProductMasterSidebar_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./ProductMasterSidebar.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/product-master/ProductMasterSidebar.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ProductMasterSidebar_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/src/views/pages/product-master/ProductMasterSidebar.vue?vue&type=style&index=0&id=fa7dd856&lang=scss&scoped=true&":
/*!****************************************************************************************************************************************!*\
  !*** ./resources/js/src/views/pages/product-master/ProductMasterSidebar.vue?vue&type=style&index=0&id=fa7dd856&lang=scss&scoped=true& ***!
  \****************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_ProductMasterSidebar_vue_vue_type_style_index_0_id_fa7dd856_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/style-loader!../../../../../../node_modules/css-loader!../../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../../node_modules/postcss-loader/src??ref--8-2!../../../../../../node_modules/sass-loader/dist/cjs.js??ref--8-3!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./ProductMasterSidebar.vue?vue&type=style&index=0&id=fa7dd856&lang=scss&scoped=true& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/product-master/ProductMasterSidebar.vue?vue&type=style&index=0&id=fa7dd856&lang=scss&scoped=true&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_ProductMasterSidebar_vue_vue_type_style_index_0_id_fa7dd856_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_ProductMasterSidebar_vue_vue_type_style_index_0_id_fa7dd856_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_ProductMasterSidebar_vue_vue_type_style_index_0_id_fa7dd856_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_ProductMasterSidebar_vue_vue_type_style_index_0_id_fa7dd856_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_2_node_modules_sass_loader_dist_cjs_js_ref_8_3_node_modules_vue_loader_lib_index_js_vue_loader_options_ProductMasterSidebar_vue_vue_type_style_index_0_id_fa7dd856_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/src/views/pages/product-master/ProductMasterSidebar.vue?vue&type=template&id=fa7dd856&scoped=true&":
/*!*************************************************************************************************************************!*\
  !*** ./resources/js/src/views/pages/product-master/ProductMasterSidebar.vue?vue&type=template&id=fa7dd856&scoped=true& ***!
  \*************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ProductMasterSidebar_vue_vue_type_template_id_fa7dd856_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./ProductMasterSidebar.vue?vue&type=template&id=fa7dd856&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/pages/product-master/ProductMasterSidebar.vue?vue&type=template&id=fa7dd856&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ProductMasterSidebar_vue_vue_type_template_id_fa7dd856_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ProductMasterSidebar_vue_vue_type_template_id_fa7dd856_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);