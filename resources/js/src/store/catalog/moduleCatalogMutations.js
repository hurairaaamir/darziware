/*=========================================================================================
  File Name: moduleCatalogMutations.js
  Description: Catalog Module
  ----------------------------------------------------------------------------------------
  Item ZainFit Vue Laravel Admin Panel Developed By RigRex
  Author: RigRex - Ahmad
  Author URL: https://www.rigrex.com
==========================================================================================*/


export default {
    ADD_CATALOG(state, catalog) {
      state.catalogs.unshift(catalog)
    },
    SET_CATALOGS(state, catalogs) {
      //   console.log(catalogs);
      state.catalogs = catalogs.catalogs
    },

    SET_CATALOG_ITEMS(state, items) {
        // console.log(items);
      state.catalogItems = items;
    },

    UPDATE_CATALOG(state, catalog) {
        const catalogIndex = state.catalogs.findIndex((c) => c.id == catalog.id)
        Object.assign(state.catalogs[catalogIndex], catalog)
    },
    REMOVE_CATALOG(state, catalogId) {
        const catalogIndex = state.catalogs.findIndex((c) => c.id == catalogId)
        state.catalogs.splice(catalogIndex, 1)
    },
    CHANGE_STATUS(state,data){
      data.forEach(element => {
        const itemIndex = state.catalogs.findIndex((i) => i.id == element.id)
        let status=(element.status=="inactive")?"active":"inactive";
        state.catalogs[itemIndex].status=status;
      });
    }
  }
