/*=========================================================================================
  File Name: moduleCatalog.js
  Description: Catalog Module
  ----------------------------------------------------------------------------------------
  Item ZainFit Vue Laravel Admin Panel Developed By RigRex
  Author: RigRex - Ahmad
  Author URL: https://www.rigrex.com
==========================================================================================*/


import state from './moduleCatalogState.js'
import mutations from './moduleCatalogMutations.js'
import actions from './moduleCatalogActions.js'
import getters from './moduleCatalogGetters.js'

export default {
  isRegistered: false,
  namespaced: true,
  state: state,
  mutations: mutations,
  actions: actions,
  getters: getters
}

