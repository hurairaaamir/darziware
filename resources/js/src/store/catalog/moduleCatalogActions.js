/*=========================================================================================
  File Name: moduleCatalogActions.js
  Description: Catalog Module
  ----------------------------------------------------------------------------------------
  Catalog ZainFit Vue Laravel Admin Panel Developed By RigRex
  Author: RigRex - Ahmad
  Author URL: https://www.rigrex.com
==========================================================================================*/

import axios from "@/axios.js"

export default {
    // ADD CATALOG
    addCatalog(context, catalog) {
      axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');

        return new Promise((resolve, reject) => {
        axios.post("/api/admin/catalog-master/add-catalog", {catalog: catalog})
            .then((response) => {
                console.log(response);
                context.commit('ADD_CATALOG', Object.assign(response.data.catalog, {id: response.data.id}))
            resolve(response)
            })
            .catch((error) => { reject(error) })
        })
    },
  //FetchCatalogs
  fetchCatalogs(context) {
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');

    return new Promise((resolve, reject) => {
      axios.get("/api/admin/catalog-master/get-catalogs")
        .then((response) => {
            // console.log(response);
            context.commit('SET_CATALOGS', response.data)
          resolve(response)
        })
        .catch((error) => { reject(error) })
    })
  },

  //FETCH ITEMS
  fetchItems(context) {
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');

    return new Promise((resolve, reject) => {
      axios.get("/api/admin/catalog-master/get-items")
        .then((response) => {
          context.commit('SET_CATALOG_ITEMS', response.data)
          resolve(response)
        })
        .catch((error) => { reject(error) })
    })
  },


  // UPDATE CATALOG
    updateCatalog(context, catalog) {

      axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');

        return new Promise((resolve, reject) => {
        axios.post(`/api/admin/catalog-master/update-catalog/${catalog.id}`, {catalog: catalog})
            .then((response) => {
              context.commit('UPDATE_CATALOG', response.data.catalog)
            resolve(response)
            })
            .catch((error) => { reject(error) })
        })
    },

  //REMOVE CATALOG
  removeCatalog(context, catalogId) {
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');

    return new Promise((resolve, reject) => {
      axios.delete(`/api/admin/catalog-master/remove-catalog/${catalogId}`)
        .then((response) => {
          context.commit('REMOVE_CATALOG', catalogId)
          resolve(response)
        })
        .catch((error) => { reject(error) })
    })
  },
  toggleStatus(context, data) {
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');

    return new Promise((resolve, reject) => {
    axios.post(`/api/admin/catalog-master/status-catalog`,data)
        .then((response) => {
            context.commit('CHANGE_STATUS', response.data.data)
            resolve(response)
        })
        .catch((error) => { reject(error) })
    })
},
}
