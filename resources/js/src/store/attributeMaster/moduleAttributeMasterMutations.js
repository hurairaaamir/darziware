/*=========================================================================================
  File Name: moduleAttributeMasterMutations.js
  Description: AttributeMaster Module
  ----------------------------------------------------------------------------------------
  AttributeMaster ZainFit Vue Laravel Admin Panel Developed By RigRex
  Author: RigRex - Ahmad
  Author URL: https://www.rigrex.com
==========================================================================================*/


export default {
    ADD_ATTRIBUTE(state, productAttribute) {
      state.productAttributes.unshift(productAttribute)
    },
    SET_ATTRIBUTES(state, productAttributes) {
      //   console.log(productAttributes);
      state.productAttributes = productAttributes.productAttributes
    },
    SET_ATTRIBUTE_ITEMS(state, attributeItems) {
      state.attributeItems = attributeItems.attributeItems
    },


    UPDATE_ATTRIBUTE(state, productAttribute) {
        const productAttributeIndex = state.productAttributes.findIndex((c) => c.id == productAttribute.id)
        Object.assign(state.productAttributes[productAttributeIndex], productAttribute)
    },
    REMOVE_ATTRIBUTE(state, productAttributeId) {
        const productAttributeIndex = state.productAttributes.findIndex((c) => c.id == productAttributeId)
        state.productAttributes.splice(productAttributeIndex, 1)
    },
  }
