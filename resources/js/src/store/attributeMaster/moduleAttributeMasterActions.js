/*=========================================================================================
  File Name: moduleAttributeMasterActions.js
  Description: AttributeMaster Module
  ----------------------------------------------------------------------------------------
  AttributeMaster ZainFit Vue Laravel Admin Panel Developed By RigRex
  Author: RigRex - Ahmad
  Author URL: https://www.rigrex.com
==========================================================================================*/

import axios from "@/axios.js"

export default {
    // ADD ATTRIBUTE
    addAttribute(context, attribute) {
      axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');

        return new Promise((resolve, reject) => {
        axios.post("/api/admin/attribute-master/add-attribute", {productAttribute: attribute})
            .then((response) => {
                console.log(response);
                context.commit('ADD_ATTRIBUTE', Object.assign(response.data.productAttribute, {id: response.data.id}))
            resolve(response)
            })
            .catch((error) => { reject(error) })
        })
    },
  //FetchAttributes
  fetchAttributes(context) {

    axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');

    return new Promise((resolve, reject) => {
      axios.get("/api/admin/attribute-master/get-attributes")
        .then((response) => {
            // console.log(response);
            context.commit('SET_ATTRIBUTES', response.data)
          resolve(response)
        })
        .catch((error) => { reject(error) })
    })
  },

  //FETCH ATTRIBUTE ITEMS
  fetchAttributeItems(context) {

    axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');

    return new Promise((resolve, reject) => {
      axios.get("/api/admin/attribute-master/get-attribute-items")
        .then((response) => {
          context.commit('SET_ATTRIBUTE_ITEMS', response.data)
          resolve(response)
        })
        .catch((error) => { reject(error) })
    })
  },



  // UPDATE ATTRIBUTE
    updateAttribute(context, attribute) {

      axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');

        return new Promise((resolve, reject) => {
        axios.post(`/api/admin/attribute-master/update-attribute/${attribute.id}`, {productAttribute: attribute})
            .then((response) => {
              context.commit('UPDATE_ATTRIBUTE', response.data.productAttribute)
            resolve(response)
            })
            .catch((error) => { reject(error) })
        })
    },

  //REMOVE ATTRIBUTE
  removeAttribute(context, attributeId) {

    axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');

    return new Promise((resolve, reject) => {
      axios.delete(`/api/admin/attribute-master/remove-attribute/${attributeId}`)
        .then((response) => {
          context.commit('REMOVE_ATTRIBUTE', attributeId)
          resolve(response)
        })
        .catch((error) => { reject(error) })
    })
  },
}
