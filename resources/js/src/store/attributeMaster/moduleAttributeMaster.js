/*=========================================================================================
  File Name: moduleAttributeMaster.js
  Description: AttributeMaster Module
  ----------------------------------------------------------------------------------------
  Item ZainFit Vue Laravel Admin Panel Developed By RigRex
  Author: RigRex - Ahmad
  Author URL: https://www.rigrex.com
==========================================================================================*/


import state from './moduleAttributeMasterState.js'
import mutations from './moduleAttributeMasterMutations.js'
import actions from './moduleAttributeMasterActions.js'
import getters from './moduleAttributeMasterGetters.js'

export default {
  isRegistered: false,
  namespaced: true,
  state: state,
  mutations: mutations,
  actions: actions,
  getters: getters
}

