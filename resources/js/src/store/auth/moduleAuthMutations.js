/*=========================================================================================
  File Name: moduleCatalogMutations.js
  Description: Catalog Module
  ----------------------------------------------------------------------------------------
  Item ZainFit Vue Laravel Admin Panel Developed By RigRex
  Author: RigRex - Ahmad
  Author URL: https://www.rigrex.com
==========================================================================================*/


export default {
  retrieveToken(state, token) {
    state.token = token
  },
  setUser(state,user){
    state.user=user;
    console.log(user);
    console.log(state.user);

    
  },
  destroyToken(state){
    state.token=null;
  }
}
