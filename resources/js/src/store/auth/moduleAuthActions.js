/*=========================================================================================
  File Name: moduleCatalogActions.js
  Description: Catalog Module
  ----------------------------------------------------------------------------------------
  Catalog ZainFit Vue Laravel Admin Panel Developed By RigRex
  Author: RigRex - Ahmad
  Author URL: https://www.rigrex.com
==========================================================================================*/

import axios from "@/axios.js"

export default {
  retrieveToken(context, credentials) {
    return new Promise((resolve, reject) => {
      axios.post('/api/login', {
        email: credentials.email,
        password: credentials.password,
      })
        .then(response =>{
          const token = response.data.access_token;
          localStorage.setItem('access_token', token);
          context.commit('retrieveToken', token);
          resolve(response);
        })
        .catch(error => {
          console.log(error)
          reject(error)
        })
      })
  },
  register(context,form){
    return new Promise((resolve,reject)=>{
      axios.post('/api/register',{
        email:form.email,
        name:form.name,
        password:form.password
      }).then((response)=>{
        console.log(response.data.data);
        context.commit('setUser',response.data.data);
        resolve(response);
      })
      .catch((error)=>{
        console.log(error);
        reject(error);
      })
    })
  },
  logout(context){
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token;
  
    return new Promise((resolve,reject)=>{
      axios.post('/api/logout')
        .then((response)=>{
          localStorage.removeItem('access_token');
          context.commit('destroyToken');
          resolve(response);
        }).catch((error)=>{
          reject(error);
        })
    })
  }
}
