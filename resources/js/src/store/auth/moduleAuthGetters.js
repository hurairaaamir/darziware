/*=========================================================================================
  File Name: moduleCatalogActions.js
  Description: Catalog Module
  ----------------------------------------------------------------------------------------
  Item ZainFit Vue Laravel Admin Panel Developed By RigRex
  Author: RigRex - Ahmad
  Author URL: https://www.rigrex.com
==========================================================================================*/


export default {
    // getItem: state => (productId) => state.products.find((product) => product.id == productId),

    loggedIn(state){
      return state.token !== null;
    }
  }
