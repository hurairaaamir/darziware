/*=========================================================================================
  File Name: moduleProductMasterActions.js
  Description: ProductMaster Module
  ----------------------------------------------------------------------------------------
  ProductMaster ZainFit Vue Laravel Admin Panel Developed By RigRex
  Author: RigRex - Ahmad
  Author URL: https://www.rigrex.com
==========================================================================================*/

import axios from "@/axios.js";

export default {
    // ADD PRODUCT
    addProduct(context, product) {

        axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');
        return new Promise((resolve, reject) => {
            axios
                .post("/api/admin/product-master/add-product", product, {
                    headers: { 'Content-Type': 'multipart/form-data' }
                })
                .then(response => {
                    console.log(response.data.product);
                    context.commit(
                        "ADD_PRODUCT",
                        Object.assign(response.data.product, { id: response.data.id })
                    );
                    resolve(response);
                })
                .catch(error => {
                    reject(error);
                });
        });
    },
    //FETCH PRODUCTS
    fetchProducts(context) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');

        return new Promise((resolve, reject) => {
            axios
                .get("/api/admin/product-master/get-products")
                .then(response => {
                    // console.log(response);
                    context.commit("SET_PRODUCTS", response.data);
                    resolve(response);
                })
                .catch(error => {
                    reject(error);
                });
        });
    },

    //FETCH CATALOGS
    fetchCatalogs(context) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');

        return new Promise((resolve, reject) => {
            axios
                .get("/api/admin/product-master/get-catalogs")
                .then(response => {
                    // console.log(response);
                    context.commit("SET_PRODUCT_CATALOGS", response.data);
                    resolve(response);
                })
                .catch(error => {
                    reject(error);
                });
        });
    },

    //FETCH ITEMS
    fetchItems(context) {

        axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');

        return new Promise((resolve, reject) => {
            axios
                .get("/api/admin/product-master/get-items")
                .then(response => {
                    // console.log(response);
                    context.commit("SET_PRODUCT_ITEMS", response.data);
                    resolve(response);
                })
                .catch(error => {
                    reject(error);
                });
        });
    },

    //FETCH ITEM CATALOGS
    fetchItemCatalogs(context, itemId) {

        axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');

        return new Promise((resolve, reject) => {
            axios
                .get(`/api/admin/product-master/get-item-catalogs/${itemId}`)
                .then(response => {
                    // console.log(response);
                    context.commit("SET_PRODUCT_ITEM_CATALOGS", response.data);
                    resolve(response);
                })
                .catch(error => {
                    reject(error);
                });
        });
    },

    //FETCH ITEM ATTRIBUTES
    fetchItemAttributes(context, itemId) {

        axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');

        return new Promise((resolve, reject) => {
            axios
                .get(`/api/admin/product-master/get-item-attributes/${itemId}`)
                .then(response => {
                    // console.log(response);
                    context.commit("SET_PRODUCT_ITEM_ATTRIBUTES", response.data);
                    resolve(response);
                })
                .catch(error => {
                    reject(error);
                });
        });
    },

    // UPDATE PRODUCT
    updateProduct(context, product) {

        axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');

        return new Promise((resolve, reject) => {
            axios
                .post(
                    `/api/admin/product-master/update-product/${product.id}`,
                    { product: product }
                )
                .then(response => {
                    context.commit("UPDATE_PRODUCT", response.data.product);
                    resolve(response);
                })
                .catch(error => {
                    reject(error);
                });
        });
    },

    //REMOVE PRODUCT
    removeProduct(context, productId) {

        axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');

        return new Promise((resolve, reject) => {
            axios
                .delete(`/api/admin/product-master/remove-product/${productId}`)
                .then(response => {
                    context.commit("REMOVE_PRODUCT", productId);
                    resolve(response);
                })
                .catch(error => {
                    reject(error);
                });
        });
    },
    toggleStatus(context, data) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');

        return new Promise((resolve, reject) => {
        axios.post(`/api/admin/product-master/status-product`,data)
            .then((response) => {
                console.log(response.data.data);
                context.commit('CHANGE_STATUS', response.data.data)
                resolve(response)
            })
            .catch((error) => { reject(error) })
        })
    },
};
