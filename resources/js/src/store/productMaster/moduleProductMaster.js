/*=========================================================================================
  File Name: moduleProductMaster.js
  Description: ProductMaster Module
  ----------------------------------------------------------------------------------------
  Item ZainFit Vue Laravel Admin Panel Developed By RigRex
  Author: RigRex - Ahmad
  Author URL: https://www.rigrex.com
==========================================================================================*/


import state from './moduleProductMasterState.js'
import mutations from './moduleProductMasterMutations.js'
import actions from './moduleProductMasterActions.js'
import getters from './moduleProductMasterGetters.js'

export default {
  isRegistered: false,
  namespaced: true,
  state: state,
  mutations: mutations,
  actions: actions,
  getters: getters
}

