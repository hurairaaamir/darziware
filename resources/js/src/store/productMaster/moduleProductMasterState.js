/*=========================================================================================
  File Name: moduleAttributeMasterState.js
  Description: Attribute Module
  ----------------------------------------------------------------------------------------
  Item ZainFit Vue Laravel Admin Panel Developed By RigRex
  Author: RigRex - Ahmad
  Author URL: https://www.rigrex.com
==========================================================================================*/

export default {
    products: [],
    productCatalogs: [],
    productItems: [],
    itemCatalogs: [],
    itemAttributes: [],
  }
