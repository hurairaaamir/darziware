/*=========================================================================================
  File Name: moduleCatalogMutations.js
  Description: Catalog Module
  ----------------------------------------------------------------------------------------
  Item ZainFit Vue Laravel Admin Panel Developed By RigRex
  Author: RigRex - Ahmad
  Author URL: https://www.rigrex.com
==========================================================================================*/


export default {
    ADD_PRODUCT(state, product) {
      state.products.unshift(product)
    },
    SET_PRODUCTS(state, products) {
      //   console.log(products);
      state.products = products.products
    },
    SET_PRODUCT_ITEMS(state, items) {
      //   console.log(products);
      state.productItems = items.items
    },

    SET_PRODUCT_CATALOGS(state, catalogs) {
      //   console.log(products);
      state.productCatalogs = catalogs.catalogs
    },

    SET_PRODUCT_ITEM_CATALOGS(state, data) {
      //   console.log(products);
      state.itemCatalogs = data.itemCatalogs
    },

    SET_PRODUCT_ITEM_ATTRIBUTES(state, data) {
        console.log(data);
      state.itemAttributes = data.itemAttributes
    },

    UPDATE_PRODUCT(state, product) {
        const productIndex = state.products.findIndex((c) => c.id == product.id)
        Object.assign(state.products[productIndex], product)
    },
    REMOVE_PRODUCT(state, productId) {
        const productIndex = state.products.findIndex((c) => c.id == productId)
        state.products.splice(productIndex, 1)
    },

    CHANGE_STATUS(state,data){
      data.forEach(element => {
        const itemIndex = state.products.findIndex((i) => i.id == element.id)
        let status=(element.status=="inactive")?"active":"inactive";
        state.products[itemIndex].status=status;
      });
    }
  }
