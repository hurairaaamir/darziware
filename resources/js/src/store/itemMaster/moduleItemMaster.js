/*=========================================================================================
  File Name: moduleItemMaster.js
  Description: ItemMaster Module
  ----------------------------------------------------------------------------------------
  Item ZainFit Vue Laravel Admin Panel Developed By RigRex
  Author: RigRex - Ahmad
  Author URL: https://www.rigrex.com
==========================================================================================*/


import state from './moduleItemMasterState.js'
import mutations from './moduleItemMasterMutations.js'
import actions from './moduleItemMasterActions.js'
import getters from './moduleItemMasterGetters.js'

export default {
  isRegistered: false,
  namespaced: true,
  state: state,
  mutations: mutations,
  actions: actions,
  getters: getters
}

