/*=========================================================================================
  File Name: moduleCatalogMutations.js
  Description: Catalog Module
  ----------------------------------------------------------------------------------------
  Item ZainFit Vue Laravel Admin Panel Developed By RigRex
  Author: RigRex - Ahmad
  Author URL: https://www.rigrex.com
==========================================================================================*/


export default {
    ADD_ITEM(state, item) {
      state.items.unshift(item)
    },
    SET_ITEMS(state, items) {
      //   console.log(items);
      state.items = items.items
    },

    SET_DW_ITEMS(state, dwItems) {
        // console.log(dwItems);
      state.dwItems = dwItems;
    },
    // SET_LABELS(state, labels) {
    //   state.eventLabels = labels
    // },
    UPDATE_ITEM(state, item) {
        const itemIndex = state.items.findIndex((i) => i.id == item.id)
        Object.assign(state.items[itemIndex], item)
    },
    REMOVE_ITEM(state, itemId) {
        const itemIndex = state.items.findIndex((i) => i.id == itemId)
        state.items.splice(itemIndex, 1)
    },
    CHANGE_STATUS(state,data){
      data.forEach(element => {
        const itemIndex = state.items.findIndex((i) => i.id == element.id)
        let status=(element.status=="inactive")?"active":"inactive";
        state.items[itemIndex].status=status;
      });
    }
  }
