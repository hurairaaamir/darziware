/*=========================================================================================
  File Name: moduleCatalogActions.js
  Description: Catalog Module
  ----------------------------------------------------------------------------------------
  Item ZainFit Vue Laravel Admin Panel Developed By RigRex
  Author: RigRex - Ahmad
  Author URL: https://www.rigrex.com
==========================================================================================*/

import axios from "@/axios.js"

export default {
    // ADD ITEM
    addItem(context, item) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');

        return new Promise((resolve, reject) => {
        axios.post("/api/admin/item-master/add-item", {item: item})
            .then((response) => {
                console.log(response);
                context.commit('ADD_ITEM', Object.assign(item, {id: response.data.id}))
            resolve(response)
            })
            .catch((error) => { reject(error) })
        })
    },
    //FetchDataList Items
    fetchDataListItems(context) {

        axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');

        return new Promise((resolve, reject) => {
        axios.get("/api/admin/item-master/get-items")
            .then((response) => {
                context.commit('SET_ITEMS', response.data)
            resolve(response)
            })
            .catch((error) => { reject(error) })
        })
    },

    //fetchDW Items
    fetchDWItems(context) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');

        return new Promise((resolve, reject) => {
        axios.get("/api/admin/item-master/get-dw-items")
            .then((response) => {
                context.commit('SET_DW_ITEMS', response.data)
            resolve(response)
            })
            .catch((error) => { reject(error) })
        })
    },



    // UPDATE ITEM
    updateItem(context, item) {

        axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');

        return new Promise((resolve, reject) => {
        axios.post(`/api/admin/item-master/update-item/${item.id}`, {item: item})
            .then((response) => {
                context.commit('UPDATE_ITEM', response.data.item)
            resolve(response)
            })
            .catch((error) => { reject(error) })
        })
    },

    // REMOVE ITEM
    removeItem(context, itemId) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');

        return new Promise((resolve, reject) => {
        axios.delete(`/api/admin/item-master/remove-item/${itemId}`)
            .then((response) => {
                context.commit('REMOVE_ITEM', itemId)
            resolve(response)
            })
            .catch((error) => { reject(error) })
        })
    },

    toggleStatus(context, data) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');

        return new Promise((resolve, reject) => {
        axios.post(`/api/admin/item-master/status-item`,data)
            .then((response) => {
                console.log(response.data.data);
                context.commit('CHANGE_STATUS', response.data.data)
                resolve(response)
            })
            .catch((error) => { reject(error) })
        })
    },

}
