import Vue from 'vue'
import Router from 'vue-router'
import frontendRouter from './frontendRouter.js'
import backendRouter from './backendRouter.js'
import store from "./store/store.js";


var allRoutes = []
allRoutes = allRoutes.concat(frontendRouter, backendRouter)

const routes = allRoutes

Vue.use(Router)

const router = new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    scrollBehavior() {
        return { x: 0, y: 0 }
    },
    routes
})

function LOGGED_IN(){
    return localStorage.getItem('access_token') !== null;
}
router.beforeEach((to, from, next) => {
    if(to.matched.some(record=> record.meta.requireAuth)){
        if(LOGGED_IN()){
            next();
        }else{
            next({name:'page-login'});            
        }
    } else {
        next();
    }
});

router.afterEach(() => {
    // Remove initial loading
    const appLoading = document.getElementById('loading-bg')
    if (appLoading) {
        appLoading.style.display = "none";
    }
})

export default router
