/*=========================================================================================
  File Name: sidebarItems.js
  Description: Sidebar Items list. Add / Remove menu items from here.
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/


export default [
  {
    url: "/admin/dashboard",
    name: "dashboard",
    slug: "dashboard",
    icon: "HomeIcon",
  },
  // {
  //   url: "/page2",
  //   name: "Page 2",
  //   slug: "page2",
  //   icon: "FileIcon",
  // },
  {
    url: null,
    name: "Store",
    icon: "ShoppingCartIcon",
    i18n: "Store",
    submenu: [
      {
        url: '/admin/item-master',
        name: "Item Master",
        slug: "item-master",
        i18n: "ItemMaster",
      },
      {
        url: '/admin/attribute-master',
        name: "Attribute Master",
        slug: "attribute-master",
        i18n: "AttributeMaster",
      },
      {
        url: '/admin/catalog-master',
        name: "Catalog Master",
        slug: "catalog-master",
        i18n: "CatalogMaster",
      },
      {
        url: '/admin/product-master',
        name: "Product Master",
        slug: "product-master",
        i18n: "ProductMaster",
      },

    ]
  },
  {
    url: "/admin/email/contact",
    name: "NewsLetter & contact",
    slug: "email",
    icon: "MailIcon",
    i18n: "Email",
  },
  {
    url: '/admin/user-settings',
    slug: 'page-user-settings',
    name: "Settings",
    icon: "SettingsIcon",
    i18n: "UserSettings",
  },
]
