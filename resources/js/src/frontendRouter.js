const routes = [
    {
        path: '',
        component: () => import('./layouts/full-page/FullPage.vue'),
        children: [
            {
                path:'/',
                name: 'page-login',
                component:require('./views/pages/auth/Login.vue').default,      
            },
            {
                path: '/pages/login',
                name: 'page-login',
                component: () => import('@/views/pages/auth/Login.vue')
            },
            {
                path:'/logout',
                name: 'logout',
                component:require('./views/pages/auth/Logout.vue').default,      
            },
            {
                path: '/pages/error-404',
                name: 'page-error-404',
                component: () => import('@/views/pages/Error404.vue')
            },
            
        ]
    },
]
  
export default routes