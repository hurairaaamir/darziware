const routes = [
    {
        path: '',
        component: () => import('./layouts/main/Main.vue'),
        children: [
            {
                path: '/admin/item-master',
                name: 'products-item-master',
                component: () => import('@/views/pages/item-master/ItemMaster.vue'),
                meta: {
                    requireAuth:true ,
                    breadcrumb: [
                        { title: 'Home', url: '/admin' },
                        { title: 'Store'},
                        { title: 'Item Master', active: true },
                    ],
                    pageTitle: 'Item Master',
                    rule: 'editor'
                },
            },
            {
                
                path: '/admin/attribute-master',
                name: 'products-attribute-master',
                component: () => import('@/views/pages/attribute-master/AttributeMaster.vue'),
                meta: {
                    requireAuth:true ,
                    breadcrumb: [
                        { title: 'Home', url: '/admin' },
                        { title: 'Store'},
                        { title: 'Attribute Master', active: true },
                    ],
                    pageTitle: 'Attribute Master',
                    rule: 'editor'
                },
            },
            {
                path: '/admin/catalog-master',
                name: 'products-catalog-master',
                component: () => import('@/views/pages/catalog-master/CatalogMaster.vue'),
                meta: {
                    requireAuth:true ,
                    breadcrumb: [
                        { title: 'Home', url: '/admin' },
                        { title: 'Store'},
                        { title: 'Catalog Master', active: true },
                    ],
                    pageTitle: 'Catalog Master',
                    rule: 'editor'
                },
            },
            {
                path: '/admin/product-master',
                name: 'products-master',
                component: () => import('@/views/pages/product-master/ProductMaster.vue'),
                meta: {
                    requireAuth:true ,
                    breadcrumb: [
                        { title: 'Home', url: '/admin' },
                        { title: 'Store'},
                        { title: 'Product Master', active: true },
                    ],
                    pageTitle: 'Product Master',
                    rule: 'editor'
                },
            },
            {
                path: '/admin/dashboard',
                name: 'dashboard',
                component: () => import('@/views/pages/dashboard/dashboard.vue'),
                meta: {
                    requireAuth:true ,
                    rule: 'editor',
                }
            },
            {
                path:'/admin/email/:filter',
                name:'email',
                component:()=> import('@/views/pages/email/Email.vue'),
                meta: {
                    requireAuth:true ,
                    breadcrumb: [
                        { title: 'Home', url: '/' },
                        { title: 'Pages' },
                        { title: 'NewsLetter & contact', active: true },
                    ],
                    pageTitle: 'Settings',
                    rule: 'editor'
                }
            },
            {
                path: '/apps/email',
                redirect: '/admin/email/inbox',
                name: 'email',
            },
            {
                path: '/admin/user-settings',
                name: 'page-user-settings',
                component: () => import('@/views/pages/user-settings/UserSettings.vue'),
                meta: {
                    breadcrumb: [
                        { title: 'Home', url: '/' },
                        { title: 'Pages' },
                        { title: 'Settings', active: true },
                    ],
                    pageTitle: 'Settings',
                    rule: 'editor'
                },
            },

        ]
    },
]

  
export default routes